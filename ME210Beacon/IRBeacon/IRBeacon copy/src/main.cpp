//Teensy SPI based code to turn on TLC5928 Driver
// Variable PWM duty cycle
// Verified up to 10kHz with 10% duty cycle steps

// 2/22/20 modified to use the PWM function on the blank
// line to generate the drive to the LEDs jec

// 2/29/19 Modified for xbee and color sensor working all together JJL

 
#include <SPI.h>
#include <IntervalTimer.h>
#include <Arduino.h>
#include <Wire.h>
#include "Adafruit_TCS34725.h"
#include <XBee.h>

//#define VERBOSE

// List of miners
enum minerNumbers {RED1, RED2, BLUE1, BLUE2};

#define THIS_MINER_NUMBER RED1

// Latch (TO driver) Pin D2
#define PIN_LAT 10

// Blank (TO driver) Pin D8
#define PIN_BLANK 21

// Status Pins
#define FAIL_THERMAL_PIN 19
#define FAIL_LED_PIN 20
#define FAIL_VOLTAGE_PIN 22
#define PIN_POWER_ON 23

// Voltage Measurement
#define V_MIN_BITS 305  // corresponds to 6.5 volts through 1/5 voltage divider
#define PIN_V_MEAS 14   // voltage meas threshold

//I2C Pins
#define PIN_SDA 17
#define PIN_SCL 16

//XBee Defines
#define PAYLOAD_SIZE      9
#define MINER_NUM_OFFSET  0
#define R_HIGH_OFFSET     1
#define R_LOW_OFFSET      2
#define G_HIGH_OFFSET     3
#define G_LOW_OFFSET      4
#define B_HIGH_OFFSET     5
#define B_LOW_OFFSET      6
#define C_HIGH_OFFSET     7
#define C_LOW_OFFSET      8
#define OPTIONS           0x00
#define FRAME_ID          0x02
#define FIELD_ADDRESS     0x3181


// Declare Module Variables
static volatile int failStatus;       // Status code returned by LED driver

static const int allOnLED =   0xFFFF;   // Bit sequence for 16 LEDs on
static const int allOffLED =  0x0000;   // Bit sequence for 16 LEDs off

static volatile int statusCode;       // Instantaneous status code 
static IntervalTimer colorQuery;      // Timer for top half of LEDs 
static IntervalTimer resetStatus;     // Timer for checking the status

static long microsPerSec =  1000000;   // Constant

static volatile int onBits = 0x0000;  // Buffer for bits that are enabled at each latch

static int currentVoltageBits;        // Voltage measurement

// Frequencies of miners in Hz
static const float minerFrequencies[] = {3333.333, 2000.000, 1428.571, 909.091};

XBee xbee = XBee(); //Create the XBee object

uint8_t payload[PAYLOAD_SIZE]; // Array of data to send to Field

uint16_t redSensor, greenSensor, blueSensor, clearSensor; // Raw RGB readings of color 
volatile boolean readFlag = false;                        // Flag for when to read color sensor and send data

Adafruit_TCS34725 SENSOR = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_154MS, TCS34725_GAIN_4X);

// Function Prototypes
void initLEDs();
void latchLED();
void flipOnOffLED();
void blankFlashLED();
void checkStatus();
void checkVoltage();
void updateBeaconParams();
void respondFailStatus();
void clearFailStatus();
void readColorSensor();
void sendXBeePacket();
void initColorSensor();
void setReadFlag();
uint8_t sixteenToEightMSB (uint16_t val);
uint8_t sixteenToEightLSB (uint16_t val);

void setup()
{
  // Initialize Serial for Debugging
  Serial.begin(9600);
  
  // Initialize Serial 3 (UART3) for XBee communication
  Serial3.begin(9600);
  xbee.setSerial(Serial3);

  initLEDs();
  initColorSensor();

  // Set up timers for frequencies and periodic status checks
  resetStatus.begin(checkStatus, microsPerSec / 2);
  colorQuery.begin(setReadFlag, microsPerSec / 2);
}


void loop() {
  // Interpret failStatus returned by LED driver every second
  // and set fault LEDs as needed.
  // Check for low voltage and disable LED output if needed
  checkVoltage();

  if (readFlag) {
    readColorSensor();
    sendXBeePacket();
    readFlag = false;
  }
}


// Check the voltage and respond if below threshold
void checkVoltage() {
    currentVoltageBits = analogRead(PIN_V_MEAS);
  if (currentVoltageBits < V_MIN_BITS) {
    digitalWrite(PIN_BLANK, HIGH);        // Disable IR LEDs
    digitalWrite(FAIL_VOLTAGE_PIN, HIGH); // Turn on LowVoltage indicator
  }
  else {
    digitalWrite(PIN_BLANK, LOW);
    digitalWrite(FAIL_VOLTAGE_PIN, LOW);
  }
}

// Print and update status LEDs if fail status is detected
void respondFailStatus() {
  
  if (failStatus == allOnLED){
     digitalWrite(FAIL_THERMAL_PIN, HIGH);
     digitalWrite(FAIL_LED_PIN, LOW);
  }
  else if (failStatus > 0) {
    digitalWrite(FAIL_LED_PIN, HIGH);
    Serial.println("LED Fail");
  }
}

// Reset status LEDs if no fail status is detected
void clearFailStatus() {
  digitalWrite(FAIL_THERMAL_PIN, LOW);
  digitalWrite(FAIL_LED_PIN, LOW);
}

// Send a latch pulse to lock in transfered LED data
void latchLED() {
    digitalWrite(PIN_LAT, HIGH);
    digitalWrite(PIN_LAT, LOW);
}

// Momentary Blank
void blankFlashLED() {
  digitalWrite(PIN_BLANK, HIGH);
  digitalWrite(PIN_BLANK, LOW);
}

// Send current onBits data to LED shift register
// and receive back status information
void transferToLED() {
  statusCode |= SPI.transfer16(onBits); 
  // Bitwise OR preserves any error codes until they are acknowleged
  // and cleared by the main loop functions
}

// Get the (fast updating) status and save to be processed
void checkStatus() {
  failStatus = statusCode;
  statusCode = 0;
}


void readColorSensor()
{
    // Reads the RGB values into the matching variables
    SENSOR.getRawData(&redSensor, &greenSensor, &blueSensor, &clearSensor);
    #ifdef VERBOSE
      Serial.print("R: "); Serial.print(redSensor, DEC); Serial.print(" ");
      Serial.print("G: "); Serial.print(greenSensor, DEC); Serial.print(" ");
      Serial.print("B: "); Serial.print(blueSensor, DEC); Serial.print(" ");
      Serial.print("C: "); Serial.print(clearSensor, DEC); Serial.print(" ");
      Serial.println(" "); 
    #endif
}

void initLEDs() {
  // Initialize SPI Transfer to LED Driver
  SPI.begin();
  // Run at 24MHz
  SPI.beginTransaction(SPISettings(24000000, MSBFIRST, SPI_MODE0));

  // Pin Modes for auxiliary interface to LED Driver
  pinMode(PIN_LAT, OUTPUT);
  pinMode(PIN_BLANK, OUTPUT);

  // Start with LEDs disabled
  digitalWrite(PIN_BLANK, HIGH);

  // Pin modes for status LEDs
  pinMode(FAIL_THERMAL_PIN, OUTPUT);
  pinMode(FAIL_LED_PIN, OUTPUT);
  pinMode(FAIL_VOLTAGE_PIN, OUTPUT);
  pinMode(PIN_POWER_ON, OUTPUT);
  digitalWrite(FAIL_LED_PIN, LOW);
  digitalWrite(FAIL_THERMAL_PIN, LOW);

  // Pin for voltage measurement
  currentVoltageBits = analogRead(PIN_V_MEAS);

  // Set everything on to overwrite random data in LED driver latch
  SPI.transfer16(allOnLED);
  latchLED();

  analogWriteFrequency(PIN_BLANK, minerFrequencies[THIS_MINER_NUMBER]);
  analogWrite(PIN_BLANK, 225); // set DC to 10% (256- (desiredDC*256))

  // show that we think the IR emitters are on
  digitalWrite(PIN_POWER_ON, HIGH);

  // Initialize variables
  statusCode = 0;
  failStatus = 0;
}

//Prepares packet to send to field
void sendXBeePacket() {
  payload[MINER_NUM_OFFSET] = THIS_MINER_NUMBER;
  payload[R_HIGH_OFFSET]  =   sixteenToEightMSB(redSensor);
  payload[R_LOW_OFFSET]   =   sixteenToEightLSB(redSensor);
  payload[G_HIGH_OFFSET]  =   sixteenToEightMSB(greenSensor);
  payload[G_LOW_OFFSET]   =   sixteenToEightLSB(greenSensor);
  payload[B_HIGH_OFFSET]  =   sixteenToEightMSB(blueSensor);
  payload[B_LOW_OFFSET]   =   sixteenToEightLSB(blueSensor);
  payload[C_HIGH_OFFSET]  =   sixteenToEightMSB(clearSensor);
  payload[C_LOW_OFFSET]   =   sixteenToEightLSB(clearSensor);
  
  Tx16Request tx = Tx16Request(FIELD_ADDRESS, OPTIONS, payload, sizeof(payload), FRAME_ID);
  xbee.send(tx);
}

//Initializes Color Sensor
void initColorSensor() {
  //Setup I2C to the correct pins
  Wire.begin();
  Wire.setSCL(PIN_SCL);
  Wire.setSDA(PIN_SDA);

  SENSOR.begin(TCS34725_ADDRESS, &Wire); //Start sensor with this I2C object
}

//Sets flag from Interval Timer for when to read and send
void setReadFlag() {
    readFlag = true;
}

uint8_t sixteenToEightLSB (uint16_t val) {
  return val & 0xFF;
}

uint8_t sixteenToEightMSB (uint16_t val) {
  return val >> 8;
}