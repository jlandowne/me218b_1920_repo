/****************************************************************************
 Module
     ReloaderEventCheckers.h
 Description
     header file for the event checking functions
 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/18/15 11:50 jec      added #include for stdint & stdbool
 08/06/13 14:37 jec      started coding
 02/18/18       hr       modified for 218b field code
*****************************************************************************/

#ifndef ReloaderEventCheckers_H
#define ReloaderEventCheckers_H

// the common headers for C99 types
#include <stdint.h>
#include <stdbool.h>

// prototypes for event checkers

bool Check4ReloadPulse_Red(void);
bool Check4ReloadPulse_Blue(void);


#endif /* ReloderEventCheckers_H */
