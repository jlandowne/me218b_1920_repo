/****************************************************************************

  Header file for LED service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef LEDService_H
#define LEDService_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Types.h"
#include "ES_Events.h"

// Public Function Prototypes

bool InitLEDService(uint8_t Priority);
bool PostLEDService(ES_Event ThisEvent);
ES_Event RunLEDService(ES_Event ThisEvent);

#define COUNTDOWN_TIME 10000

typedef struct zone {
  uint8_t brightness; // brightness only contains 5 bits of data
  uint32_t color;
  uint8_t coordX;
  uint8_t coordY;
  bool scoring;
} zone_t;

typedef enum LEDState {
	LED_Init, LED_Standby, LED_Starting, LED_Game
} LEDState_t;

#endif

