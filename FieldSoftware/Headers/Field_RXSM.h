/****************************************************************************

  Header file FIELD_RXSM module

 ****************************************************************************/
#ifndef FIELD_RXSM
#define FIELD_RXSM

#include "ES_Configure.h"
#include "ES_Framework.h"

// typedefs for the states
// State definitions for use with the query function

typedef enum {
    WaitFor7E, WaitForMSB, WaitForLSB,
    SuckUpData
} Field_RX_State_t;

typedef struct minerStruct{uint8_t MinerVal; uint16_t RedVal; uint16_t GrnVal;
	uint16_t BluVal; uint16_t ClrVal;} minerStruct;

// Public Function Prototypes

bool InitFieldRXService(uint8_t Priority);
bool PostFieldRXService(ES_Event ThisEvent);
ES_Event RunFieldRXService(ES_Event ThisEvent);

void FieldRXIntResponse(void);
minerStruct GetLastMiner(void);

//Define struct here to be used in rest of project

#endif // FIELD_RXSM
