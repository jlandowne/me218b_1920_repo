/****************************************************************************

  Header file SuperPAC module

 ****************************************************************************/

#ifndef DEBOUNCESERVICE_H
#define DEBOUNCESERVICE_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// Public Function Prototypes

bool InitDebounceService(uint8_t Priority);
bool PostDebounceService(ES_Event ThisEvent);
ES_Event RunDebounceService(ES_Event ThisEvent);

#endif /* DISPLAYSERVICE_H */

