/****************************************************************************

  Header file SuperPAC module

 ****************************************************************************/

#ifndef TIMESERVICE_H
#define TIMESERVICE_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum { TS_Idle, TS_Counting } TimeServiceState_t ;


// Public Function Prototypes

bool InitTimeService(uint8_t Priority);
bool PostTimeService(ES_Event ThisEvent);
ES_Event RunTimeService(ES_Event ThisEvent);

uint8_t TS_getMinutesLeft(void);
uint8_t TS_getSecondsLeft(void);
uint8_t TS_getRawTimeLeft(void);
void TS_setActivity(void);
bool TS_getInactive(void);

#endif /* TIMESERVICE_H */

