/****************************************************************************

  Header file FIELDGAMESERVICE module

 ****************************************************************************/

#ifndef FIELDGAMESERVICE_H
#define FIELDGAMESERVICE_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum { WaitingForPermits, PermitsIssued, PermitsExpired} FieldGameState_t ;

typedef struct ScoringLocations { uint8_t RedLoc; 
																	uint8_t BluLoc; 
																	uint8_t Neut1Loc; 
																	uint8_t Neut2Loc; 
} ScoringLocations;

ScoringLocations GetScoringLocations(void);

// Public Function Prototypes

bool InitFieldGameService(uint8_t Priority);
bool PostFieldGameService(ES_Event ThisEvent);
ES_Event RunFieldGameService(ES_Event ThisEvent);

//Protocol Specific getter functions
uint8_t QueryFieldGameStatus(void);
uint8_t QueryPUR1(void);
uint8_t QueryPUR2(void);

FieldGameState_t QueryFieldGameState(void);
uint8_t QueryWinner(void);
uint16_t QueryRegulationStartTime(void);
uint16_t QueryTieBreakStartTime(void);
uint8_t QueryPermitLocations(uint8_t permit);

void RegualtionTimerOneShotISR( void );

#endif /* FIELDGAMESERVICE_H */

