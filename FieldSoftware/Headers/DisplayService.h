/****************************************************************************

  Header file SuperPAC module

 ****************************************************************************/

#ifndef DISPLAYSERVICE_H
#define DISPLAYSERVICE_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// Public Function Prototypes

bool InitDisplayService(uint8_t Priority);
bool PostDisplayService(ES_Event ThisEvent);
ES_Event RunDisplayService(ES_Event ThisEvent);

#endif /* DISPLAYSERVICE_H */

