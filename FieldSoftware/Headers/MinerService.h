/****************************************************************************

  Header file for Miner Service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef MINERSERVICE_H
#define MINERSERVICE_H

#include "ES_Framework.h"
#include "ES_Types.h"     /* gets bool type for returns */

typedef enum {RED_1, RED_2, BLUE_1, BLUE_2} Miners;
typedef enum  {Lavender, Maroon, Blue, Mint, Olive, Beige, Orange, Magenta, 
	Apricot, Red, Purple, Cyan, Navy, Lime, Brown, Yellow, White} Colors;

// Public Function Prototypes

bool InitMinerService(uint8_t Priority);
bool PostMinerService(ES_Event ThisEvent);
ES_Event RunMinerService(ES_Event ThisEvent);

//Setter Functions
void ResetScores (void);


//Generic getter Functions
uint16_t GetRedScore (void);
uint16_t GetBlueScore (void);
uint8_t GetMinerLocation (uint8_t Miner);

//Protocol-specific getter functions
uint8_t QueryC1MLoc1 (void);
uint8_t QueryC1MLoc2 (void);
uint8_t QueryC2MLoc1 (void);
uint8_t QueryC2MLoc2 (void);
uint16_t QueryC1Res(void);
uint16_t QueryC2Res(void);


#endif /* MinerService.h */

