/****************************************************************************

  Header file SuperPAC module

 ****************************************************************************/

#ifndef FANSERVICE_H
#define FANSERVICE_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// Public Function Prototypes

bool InitFanService(uint8_t Priority);
bool PostFanService(ES_Event ThisEvent);
ES_Event RunFanService(ES_Event ThisEvent);

void setFanDC(uint8_t _DC);
uint8_t getFanDC(void);
float getFanTemperature(void);
float getAmpTemperature(void);

#endif /* DISPLAYSERVICE_H */

