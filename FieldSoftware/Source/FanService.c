/****************************************************************************
 Module
   TimeService.c

 Description
   Main state machine for game control

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 16/01/28               dy          first draft
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for the framework and this service
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"  // Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "ES_ShortTimer.h"

#include "FanService.h"
#include "PWM10Tiva.h"
#include "ADMulti.h"

#include <math.h>

/*----------------------------- Module Defines ----------------------------*/

#define REFRESH_TIME 10000
#define MEASURE_TIME   100

#define DEFAULT_FAN_DC 15

#define AMP_INDEX 2
#define EGT_INDEX 3

#define ETA 0.01f

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

static uint8_t fanDC = DEFAULT_FAN_DC;

static float fanEGT;
static float ampT;

static uint32_t analogArray[4] = {0, 0, 0, 0};

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitSuperPAC

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     intial control used for game control
 Notes

 Author
     DY
****************************************************************************/
bool InitFanService(uint8_t Priority) {
//  ES_Event ThisEvent;

    MyPriority = Priority;

    PWM_TIVA_Init(1);

    PWM_TIVA_SetFreq(25, 0);

    ADC_MultiInit(4);

    fanEGT = 0.0f;
    ampT = 0.0f;

    ES_Timer_InitTimer(FAN_TIMER, REFRESH_TIME);
    ES_Timer_InitTimer(TEMP_TIMER, MEASURE_TIME);

    return true;
}

/****************************************************************************
 Function
     PostSuperPAC

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     DY
****************************************************************************/
bool PostFanService(ES_Event ThisEvent) {
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunSuperPAC

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
Events: ES_GAME_Start, ES_GAME_Pause, ES_GAME_End, ES_ChangeFrequency, ES_AttackEd
ES_Timeout: Game_Timer, Attack_Timer
 Notes

 Author
   DY
****************************************************************************/
ES_Event RunFanService(ES_Event ThisEvent) {
    ES_Event ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

    if (ThisEvent.EventType == ES_TIMEOUT) {
        if (ThisEvent.EventParam == FAN_TIMER) {

            PWM_TIVA_SetDuty(fanDC, 0);

            ES_Timer_InitTimer(FAN_TIMER, REFRESH_TIME);
        } else if (ThisEvent.EventParam == TEMP_TIMER) {
            float newEGT, newAmp;

            ADC_MultiRead(analogArray);

            newEGT = (analogArray[EGT_INDEX] * (3.3 / 4095.0)) * 100.0 - 50.0;
            newAmp = (analogArray[AMP_INDEX] * (3.3 / 4095.0)) * 100.0 - 50.0;

            fanEGT = ETA * newEGT + (1 - ETA) * fanEGT;
            ampT = ETA * newAmp + (1 - ETA) * ampT;

            fanDC = 10 + ((ampT - 30 > 0) ? (uint8_t)(5 * (ampT - 30)) : 0);

            ES_Timer_InitTimer(TEMP_TIMER, MEASURE_TIME);
        }
    }

    return ReturnEvent;
}

void setFanDC(uint8_t _DC) {
    fanDC = _DC;
    PWM_TIVA_SetDuty(fanDC, 0);
}

uint8_t getFanDC(void) {
    return fanDC;
}

float getFanTemperature(void) {
    return fanEGT;
}

float getAmpTemperature(void) {
    return ampT;
}

/***************************************************************************
 private functions
 ***************************************************************************/


/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

