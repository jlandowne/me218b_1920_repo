/****************************************************************************
 Module
   Field_TXSM.c

 Revision
   1.0.1

 Description
   Field Transmission Service
 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/20/20       Pablo		 Begin retrofitting TX to RX
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
 02/03/18       hr       edits for 2018 b field, comments to legacy code
 ****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
 */
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "Field_RXSM.h"
#include "sci.h"
#include "FieldGameService.h"
#include "MinerService.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"  // Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "inc/hw_uart.h"
#include "inc/hw_nvic.h"

/*----------------------------- Module Defines ----------------------------*/

#define RX_DEBUG_FLAG  0
#define RX_DEBUG if(RX_DEBUG_FLAG) printf

#define START_BYTE										0x7E

#define START_DELIMIER_OFFSET 				0x00
#define LENGTH_MSB_OFFSET 						0x01
#define LENGTH_LSB_OFFSET 						0x02
#define FRAME_TYPE_OFFSET 						0x03
#define FRAME_ID_OFFSET 							0x04
#define DESTINATION_MSB_OFFSET 				0x05
#define DESTINATION_LSB_OFFSET 				0x06
#define OPTIONS_OFFSET 								0x07
#define CHECKSUM_OFFSET 							XBEE_TXPACKET_MAX_LENGTH - 1

#define FRAME_ID 											0x01 	//Miner identificator
#define FRAME_DATA_MANDATORY_LENGTH 	5 		//API Idendifier + Address (MSB/LSB) + RSSI + Options
#define PACKET_DATA_LENGTH 						14		//Addresses 0x00 -> 0x0A
#define LENGTH_MSB 										0x00
#define LENGTH_LSB 										(FRAME_DATA_MANDATORY_LENGTH + PACKET_DATA_LENGTH)
#define XBEE_RXPACKET_MAX_LENGTH  		(LENGTH_LSB + 4) 	// Message Length + Start Delimiter + Length LSB/MSB + Checksum
#define BROADCAST_ADDRESS 						0xFF
#define OPTIONS 											0x00

#define START_DELIMITER 	0x7E
#define API_RX_DATA 			0x81
#define API_TX_STAT 			0x89
#define API_TX_REQ 				0x01
#define VALID_CKSUM 			0xFF

#define DATA_OFFSET       0x05

//Addresses of registers for data to read
#define MINER_NO 											0x00 + DATA_OFFSET
#define RED_MSW												0x01 + DATA_OFFSET
#define RED_LSW												0x02 + DATA_OFFSET
#define GRN_MSW												0x03 + DATA_OFFSET
#define GRN_LSW												0x04 + DATA_OFFSET
#define BLU_MSW												0x05 + DATA_OFFSET
#define BLU_LSW												0x06 + DATA_OFFSET
#define CLR_MSW												0x07 + DATA_OFFSET
#define CLR_LSW												0x08 + DATA_OFFSET


#define STATUS_UPDATE_PERIOD 200       // milliseconds
#define STATUS_SLOW_UPDATE_PERIOD 1000       // milliseconds
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
 */

static void StorePacket	(void);
static void StoreMiner(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

// Worst case, we're retrying all messages in the buffer
static ES_Event DeferralQueue[5];

static Field_RX_State_t currentState;


static volatile uint8_t bufferLen;

static uint8_t BytesLeft;
static uint8_t CalculatedCheckSum;
static uint8_t ReportedCheckSum;
static uint8_t PacketIndex;
static uint8_t XBeeRXBuffer[PACKET_DATA_LENGTH]; //TODO 
static uint8_t ValidPacket[PACKET_DATA_LENGTH]; //Define the frame max length

//Allocate here the miner variables
static minerStruct LastMiner = {0, 0, 0, 0, 0};


/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 Function
     InitFieldRXService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
 ****************************************************************************/
bool InitFieldRXService(uint8_t Priority) {
 
    MyPriority = Priority;

    ES_InitDeferralQueueWith(DeferralQueue, ARRAY_SIZE(DeferralQueue));
    
		//UART Init handled outside
		RX_DEBUG("Inited RX");
    //reset variables 
    bufferLen = 0;
		BytesLeft = 0;
		CalculatedCheckSum = 0;
		ReportedCheckSum = 0;
		PacketIndex = 0;
	

    currentState = WaitFor7E; //set current state to wait for receive
    
    //ES_Timer_InitTimer(STATUS_TIMER, STATUS_UPDATE_PERIOD); //start timer to broadcast message
   
    return true;
}
/****************************************************************************
 Function
     PostFieldRXService

 Parameters
     ES_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue. First in First out.
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
 ****************************************************************************/
bool PostFieldRXService(ES_Event ThisEvent) {
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
     PostLIFOFieldRXService

 Parameters
     ES_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue. Last in first out.
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
 ****************************************************************************/
bool PostLIFOFieldRXService(ES_Event ThisEvent) {
    return ES_PostToServiceLIFO(MyPriority, ThisEvent);
}
/****************************************************************************
 Function
    RunFieldRXService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
  KJ Changes: changed buffer packet from 0 to 8
              increase status update timer to be slow

 Author
   J. Edward Carryer, 01/15/12, 15:23
 ****************************************************************************/
ES_Event RunFieldRXService(ES_Event ThisEvent) {
    ES_Event ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
 
    switch (currentState) {
        case WaitFor7E: //if current state is wait2 transmit
        {
					if(ThisEvent.EventType == ES_BYTE_RECEIVED && ThisEvent.EventParam == START_BYTE) { //TODO: Add start_byte def
            //Clear variables    
						RX_DEBUG("Received 7E to start\n\r");
						BytesLeft = 0;
						CalculatedCheckSum = 0;
						ReportedCheckSum = 0;
						PacketIndex = 0;
            currentState = WaitForMSB; //change states to transmitting
            }
        }
        break;
        
        case WaitForMSB: //if current state is waiting for MSB Len
        {
            if (ThisEvent.EventType == ES_BYTE_RECEIVED) {
								BytesLeft += (ThisEvent.EventParam << 8); //Set MSB of bytes left
                currentState = WaitForLSB; //change state to wait2Transmit
            }
        }
				break;
				
				case WaitForLSB: //if current state is waiting for LSB Len
        {
            if (ThisEvent.EventType == ES_BYTE_RECEIVED) {
								BytesLeft += ThisEvent.EventParam; //Set LSB of bytes left
                currentState = SuckUpData; //change state to wait2Transmit
								//RX_DEBUG("Will process %d bytes\n\r", BytesLeft);
            }
        }
				break;
        
				case SuckUpData: //if current state is waiting for LSB Len
        {
            if (ThisEvent.EventType == ES_BYTE_RECEIVED) {
							  //Store into received packet
								XBeeRXBuffer[PacketIndex] = ThisEvent.EventParam;
								//Check for valid frame ID
							  if (XBeeRXBuffer[PacketIndex] != 0x81 && PacketIndex == 0){
									  currentState = WaitFor7E;
								}
								if (((XBeeRXBuffer[PacketIndex] & BIT1HI) == BIT1HI) && PacketIndex == 4) {
									//Packet broadcast, ignore buffer
									currentState = WaitFor7E;
								}
								PacketIndex += 1;
								//Check if bytes are done flushing
								if (BytesLeft !=   0) {
										BytesLeft -= 1;
									  CalculatedCheckSum += ThisEvent.EventParam;
								}
								else {
									//Get reported checksum
									ReportedCheckSum = ThisEvent.EventParam;
									//Check that checksum is valid
									if (CalculatedCheckSum + ReportedCheckSum == 0xff) {
											StorePacket();
										  StoreMiner();
									}
									RX_DEBUG("\n\r");
									currentState = WaitFor7E;
								}
            }
        }
				break;
				
        default:
            break;
    }
    return ReturnEvent;
}

/***************************************************************************
 Getters/Setters
 ***************************************************************************/
/****************************************************************************
 Function
   GetLastMiner

 Parameters
   none

 Returns
    miner struct

 Description
   Returns last miner stored in buffer
 Notes

 Author

 ****************************************************************************/
minerStruct GetLastMiner(void) 
{
	return LastMiner;
}

/****************************************************************************
 Function
   FieldRXIntResponse

 Parameters
   none

 Returns
    none

 Description
   RX UART interrupt response called from UART3 interrupt response
 Notes

 Author
X
 ****************************************************************************/
void FieldRXIntResponse(void) 
{
	  RX_DEBUG("Recieved data, in ISR\n\r");
    // clear the RX intrerrupt:  Set RXIC in UARTICR to clear RX interrupt (Page 933 & 934)
    HWREG(UART3_BASE + UART_O_ICR) |= UART_ICR_RXIC;
		
		// TODO: Save here the event variable
		// Post ES_TX_SEND_COMPLETE to this service
		ES_Event RXEvent;
		RXEvent.EventType = ES_BYTE_RECEIVED;
		RXEvent.EventParam = HWREG(UART3_BASE + UART_O_DR);;  //Save received frame as param
		PostFieldRXService(RXEvent);
}

/***************************************************************************
 private functions
 ***************************************************************************/
/****************************************************************************
 Function
    StoreMiner

 Parameters
   none

 Returns
   none

 Description
   Stores a miner in their corresponding struct, and 
   posts an event to update the miner location
 Notes

 Author

 ****************************************************************************/
static void StoreMiner(void) {
		//Save out the miner into the last miner struct
		LastMiner.MinerVal = ValidPacket[MINER_NO];
		LastMiner.RedVal = (ValidPacket[RED_MSW] << 8) + ValidPacket[RED_LSW];
		LastMiner.GrnVal = (ValidPacket[GRN_MSW] << 8) + ValidPacket[GRN_LSW];
		LastMiner.BluVal = (ValidPacket[BLU_MSW] << 8) + ValidPacket[BLU_LSW];
		LastMiner.ClrVal = (ValidPacket[CLR_MSW] << 8) + ValidPacket[CLR_LSW];
	RX_DEBUG("Miner No: %d, Red %4x, Blue %4x, Green %4x, Clear %4x\n\r",LastMiner.MinerVal,
	LastMiner.RedVal, LastMiner.GrnVal, LastMiner.BluVal, LastMiner.ClrVal);
	ES_Event PostEvent;
	PostEvent.EventType = ES_MINER_UPDATE;
	PostMinerService(PostEvent);
	//printf("Event posted  ");
}
/****************************************************************************
 Function
    StorePacket

 Parameters
   none

 Returns
   none

 Description
   Sends a broadcast message 
 Notes

 Author

 ****************************************************************************/
static void StorePacket	(void) {
	int i;
  for (i=0; i<PACKET_DATA_LENGTH; i++)
  {
    ValidPacket[i] = XBeeRXBuffer[i];
		RX_DEBUG("%x  ",XBeeRXBuffer[i]);
  }
    
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

