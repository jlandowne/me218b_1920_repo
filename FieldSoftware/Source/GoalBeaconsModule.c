/****************************************************************************
 Module
   GoalBeaconsModule.c

 Revision
   1.0.1

 Description
   This is a template file for implementing flat state machines under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
// Hardware
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"
#include "termio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h" 


#include "ES_Configure.h"
#include "ES_Framework.h"
#include "GoalBeaconsModule.h"
#include "PWM10Tiva.h"
#include "FieldGameService.h"

/*----------------------------- Module Defines ----------------------------*/
#define BREAK_TRIGGER_TIME 80000
#define BITS_PER_NIBBLE 4
#define PB6 0
#define PB7 1
#define GROUP_0 0
#define PULSE_FREQ 1000

//#define DEBUG_GOAL_BEACONS

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
static void InitBlueGoalDetectorA(void);
static void InitBlueGoalDetectorB(void);
static void InitRedGoalDetectorA(void);
static void InitRedGoalDetectorB(void);
static void InitGoalDetectorTimers(void);

/*---------------------------- Module Variables ---------------------------*/
//storage variables for Blue Goal A Timer
static uint32_t BlueA_Period;
static uint32_t BlueA_LastCapture;

//storage variables for Blue Goal B Timer
static uint32_t BlueB_Period;
static uint32_t BlueB_LastCapture;

//storage variables for Red Goal A Timer
static uint32_t RedA_Period;
static uint32_t RedA_LastCapture;

//storage variables for Red Goal B Timer
static uint32_t RedB_Period;
static uint32_t RedB_LastCapture;


/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitGoalBeaconDetector

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
void InitGoalBeaconsModule(void)
{
  InitGoalDetectorTimers(); //initilaize the timers used for input capture for beacon detection
  
  //Initialize the emitters
  //set the frequency to 1kHz
  PWM_TIVA_SetFreq(PULSE_FREQ, GROUP_0);
  PWM_TIVA_SetDuty(50, PB6);
  PWM_TIVA_SetDuty(50, PB7);
}
/****************************************************************************
 Function
     Blue_Goal_Detector_A_ISR

 Parameters
     none

 Returns
     none

 Description
     ISR for inputcapture Wtimer 0A, PC4
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
void Blue_Goal_Detector_A_ISR(void)
{
    uint32_t ThisCapture;
    
    //start by clearing the source of the interrupt, the input capture event
    HWREG(WTIMER0_BASE+TIMER_O_ICR) = TIMER_ICR_CAECINT;
    
    //now grab the captured value and calculate the period
    ThisCapture = HWREG(WTIMER0_BASE+TIMER_O_TAR);
    BlueA_Period = ThisCapture - BlueA_LastCapture;
    
    //update last capture to prepare for the next edge
    BlueA_LastCapture = ThisCapture;
    if(BlueA_Period > (BREAK_TRIGGER_TIME))
    {
      ES_Event NewEvent;
//      NewEvent.EventType = ES_SCORED_BLUE;
      PostFieldGameService(NewEvent);
        #ifdef DEBUG_GOAL_BEACONS
           printf("b,a!");
        #endif
    }
}
/****************************************************************************
 Function
     Blue_Goal_Detector_B_ISR

 Parameters
     none

 Returns
     none

 Description
     ISR for inputcapture Wtimer 0B, PC5
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
void Blue_Goal_Detector_B_ISR(void)
{
    uint32_t ThisCapture;
    
    //start by clearing the source of the interrupt, the input capture event
    HWREG(WTIMER0_BASE+TIMER_O_ICR) = TIMER_ICR_CBECINT;
    
    //now grab the captured value and calculate the period
    ThisCapture = HWREG(WTIMER0_BASE+TIMER_O_TBR);
    BlueB_Period = ThisCapture - BlueB_LastCapture;
    
    //update last capture to prepare for the next edge
    BlueB_LastCapture = ThisCapture;
    if(BlueB_Period > (BREAK_TRIGGER_TIME))
    {
      ES_Event NewEvent;
//      NewEvent.EventType = ES_SCORED_BLUE;
      PostFieldGameService(NewEvent);
        #ifdef DEBUG_GOAL_BEACONS
           printf("bb");
        #endif
    }
}
/****************************************************************************
 Function
     Red_Goal_Detector_A_ISR

 Parameters
     none

 Returns
     none

 Description
     ISR for inputcapture Wtimer 2A, PD0
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
void Red_Goal_Detector_A_ISR(void)
{
    uint32_t ThisCapture;
    
    //start by clearing the source of the interrupt, the input capture event
    HWREG(WTIMER2_BASE+TIMER_O_ICR) = TIMER_ICR_CAECINT;
    
    //now grab the captured value and calculate the period
    ThisCapture = HWREG(WTIMER2_BASE+TIMER_O_TAR);
    RedA_Period = ThisCapture - RedA_LastCapture;
    
    //update last capture to prepare for the next edge
    RedA_LastCapture = ThisCapture;
    if(RedA_Period > (BREAK_TRIGGER_TIME))
    {
      ES_Event NewEvent;
      //NewEvent.EventType = ES_SCORED_RED;
      PostFieldGameService(NewEvent);
        #ifdef DEBUG_GOAL_BEACONS
           printf("ra");
        #endif
    }
}
/****************************************************************************
 Function
     Red_Goal_Detector_B_ISR

 Parameters
     none

 Returns
     none

 Description
     ISR for inputcapture Wtimer 2B, PD1
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/

void Red_Goal_Detector_B_ISR(void)
{
    uint32_t ThisCapture;
    
    //start by clearing the source of the interrupt, the input capture event
    HWREG(WTIMER2_BASE+TIMER_O_ICR) = TIMER_ICR_CBECINT;
    
    //now grab the captured value and calculate the period
    ThisCapture = HWREG(WTIMER2_BASE+TIMER_O_TBR);
    RedB_Period = ThisCapture - RedB_LastCapture;
    
    //update last capture to prepare for the next edge
    RedB_LastCapture = ThisCapture;
    if(RedB_Period > (BREAK_TRIGGER_TIME))
    {
      ES_Event NewEvent;
//      NewEvent.EventType = ES_SCORED_RED;
      PostFieldGameService(NewEvent);
        #ifdef DEBUG_GOAL_BEACONS
           printf("rb");
        #endif
    }
}
/***************************************************************************
 private functions
 ***************************************************************************/
/****************************************************************************
 Function
   InitGoalDetectorTimers

 Parameters
   None

 Returns
   Nothing

 Description
   Called during initialization sequences (game startup)
   to configure the wide timer interrupts on each Goal Beam Break 
   Detector (Blue A and B, Red A and B).
 
 Author
   Brett Glasner
****************************************************************************/
static void InitGoalDetectorTimers(void)
{
  //init all the input captures for the detectors
  InitBlueGoalDetectorA(); 
  InitBlueGoalDetectorB();
  InitRedGoalDetectorA();
  InitRedGoalDetectorB();
}

/****************************************************************************
 Function
   InitBlueGoalDetectorA

 Parameters
   None

 Returns
   Nothing

 Description
   Called during initialization sequences by InitGoalDetectorTimers
   to initialize the WideTimer0 Timer A interrupt on PC4.
 
 Author
   Brett Glasner
****************************************************************************/
static void InitBlueGoalDetectorA(void)
{
    //start by enabling the clock to Wide Timer 0
    HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
    
    //enable the clock to Port C
    HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
    //we don't need to delay configuring the timer now that we have executed another line
    
    //make sure that Timer A is disabled before continuing
    HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
    
    //configure 32 bit wide mode
    HWREG(WTIMER0_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
    
    //we want to use full 32 bit count so initialize Interval Load Register
    //to 0xffffffff
    HWREG(WTIMER0_BASE+TIMER_O_TAILR) = 0xffffffff;
    
    //set up timer A in capture mode (TAMR = 3, TAAMS = 0),
    //for edge time (TACMR = 1) and up-counting (TACDIR = 1)
    HWREG(WTIMER0_BASE+TIMER_O_TAMR) = 
        (HWREG(WTIMER0_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) |
            (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
    
    //set event to on rising edge
    HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
    
    //now set up port to do capture
    //set alternate function on pin PC4
    HWREG(GPIO_PORTC_BASE+GPIO_O_AFSEL) |= BIT4HI;
    
    HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) =
        (HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0xfff0ffff) + (7<<(4*BITS_PER_NIBBLE));
    
    //enable pin on port c for digital IO
    HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= BIT4HI;
    //enable pin on port c as input
    HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= BIT4LO;
    
    //enable local capture interrupt on the timer
    HWREG(WTIMER0_BASE+TIMER_O_IMR) |= TIMER_IMR_CAEIM;
    
    //enable timer A in wide timer 0 to interrup in the NVIC
    //interrupt #94, BIT30 on EN2
    HWREG(NVIC_EN2) |= BIT30HI;
    
    //make sure interrupts are enable globally
    __enable_irq();
    
    //kick off the timer by enabling it and enable timer to stop while stalled in the debugger
    HWREG(WTIMER0_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
       
}
/****************************************************************************
 Function
   InitBlueGoalDetectorB

 Parameters
   None

 Returns
   Nothing

 Description
   Called during initialization sequences by InitGoalDetectorTimers
   to initialize the WideTimer0 Timer B interrupt on PC5.
 
 Author
   Brett Glasner
****************************************************************************/
static void InitBlueGoalDetectorB(void)
{
    //start by enabling the clock to Wide Timer 0
    HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
    
    //enable the clock to Port C
    HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
    //we don't need to delay configuring the timer now that we have executed another line
    while ((HWREG(SYSCTL_PRGPIO) & BIT2HI) != BIT2HI)
    
    //make sure that Timer B is disabled before continuing
    HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
    
    //configure 32 bit wide mode
    HWREG(WTIMER0_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
    
    //we want to use full 32 bit count so initialize Interval Load Register
    //to 0xffffffff
    HWREG(WTIMER0_BASE+TIMER_O_TAILR) = 0xffffffff;
    
    //set up timer B in capture mode (TBMR = 3, TBAMS = 0),
    //for edge time (TBCMR = 1) and up-counting (TBCDIR = 1)
    HWREG(WTIMER0_BASE+TIMER_O_TBMR) = 
        (HWREG(WTIMER0_BASE+TIMER_O_TBMR) & ~TIMER_TBMR_TBAMS) |
            (TIMER_TBMR_TBCDIR | TIMER_TBMR_TBCMR | TIMER_TBMR_TBMR_CAP);
    
    //set event to on rising edge
    HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEVENT_M;
    
    //now set up port to do capture
    //set alternate function on pin PC5
    HWREG(GPIO_PORTC_BASE+GPIO_O_AFSEL) |= BIT5HI;
    
    HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) =
        (HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0xff0fffff) + (7<<(5*BITS_PER_NIBBLE));
    
    //enable pin on port c for digital IO
    HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= BIT5HI;
    //enable pin on port c as input
    HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= BIT5LO;
    
    //enable local capture interrupt on the timer
    HWREG(WTIMER0_BASE+TIMER_O_IMR) |= TIMER_IMR_CBEIM;
    
    //enable timer B in wide timer 0 to interrup in the NVIC
    //interrupt #95, BIT31 on EN2
    HWREG(NVIC_EN2) |= BIT31HI;
    
    //make sure interrupts are enable globally
    __enable_irq();
    
    //kick off the timer by enabling it and enable timer to stop while stalled in the debugger
    HWREG(WTIMER0_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}

/****************************************************************************
 Function
   InitRedGoalDetectorA

 Parameters
   None

 Returns
   Nothing

 Description
   Called during initialization sequences by InitGoalDetectorTimers
   to initialize the WideTimer2 Timer A interrupt on PD0.
 
 Author
   Brett Glasner
****************************************************************************/
static void InitRedGoalDetectorA(void)
{
      //start by enabling the clock to Wide Timer 2
    HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R2;
    
    //enable the clock to Port D
    HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R3;
    //we don't need to delay configuring the timer now that we have executed another line
    
    //make sure that Timer A is disabled before continuing
    HWREG(WTIMER2_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
    
    //configure 32 bit wide mode
    HWREG(WTIMER2_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
    
    //we want to use full 32 bit count so initialize Interval Load Register
    //to 0xffffffff
    HWREG(WTIMER2_BASE+TIMER_O_TAILR) = 0xffffffff;
    
    //set up timer A in capture mode (TAMR = 3, TAAMS = 0),
    //for edge time (TACMR = 1) and up-counting (TACDIR = 1)
    HWREG(WTIMER2_BASE+TIMER_O_TAMR) = 
        (HWREG(WTIMER2_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) |
            (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
    
    //set event to on rising edge
    HWREG(WTIMER2_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
    
    //now set up port to do capture
    //set alternate function on pin PD0
    HWREG(GPIO_PORTD_BASE+GPIO_O_AFSEL) |= BIT0HI;
    
    HWREG(GPIO_PORTD_BASE+GPIO_O_PCTL) =
        (HWREG(GPIO_PORTD_BASE+GPIO_O_PCTL) & 0xfffffff0) + (7<<(0*BITS_PER_NIBBLE));
    
    //enable pin on port d for digital IO
    HWREG(GPIO_PORTD_BASE+GPIO_O_DEN) |= BIT0HI;
    //enable pin on port c as input
    HWREG(GPIO_PORTD_BASE+GPIO_O_DIR) &= BIT0LO;
    
    //enable local capture interrupt on the timer
    HWREG(WTIMER2_BASE+TIMER_O_IMR) |= TIMER_IMR_CAEIM;
    
    //enable timer A in wide timer 0 to interrup in the NVIC
    //interrupt #98, BIT2 on EN3
    HWREG(NVIC_EN3) |= BIT2HI;
    
    //make sure interrupts are enable globally
    __enable_irq();
    
    //kick off the timer by enabling it and enable timer to stop while stalled in the debugger
    HWREG(WTIMER2_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}

/****************************************************************************
 Function
   InitRedGoalDetectorB

 Parameters
   None

 Returns
   Nothing

 Description
   Called during initialization sequences by InitGoalDetectorTimers
   to initialize the WideTimer2 Timer B interrupt on PD1.
 
 Author
   Brett Glasner
****************************************************************************/
static void InitRedGoalDetectorB(void)
{
      //start by enabling the clock to Wide Timer 2
    HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R2;
    
    //enable the clock to Port D
    HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R3;
    //we don't need to delay configuring the timer now that we have executed another line
    while ((HWREG(SYSCTL_PRGPIO) & BIT3HI) != BIT3HI)
    
    //make sure that Timer B is disabled before continuing
    HWREG(WTIMER2_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
    
    //configure 32 bit wide mode
    HWREG(WTIMER2_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
    
    //we want to use full 32 bit count so initialize Interval Load Register
    //to 0xffffffff
    HWREG(WTIMER2_BASE+TIMER_O_TAILR) = 0xffffffff;
    
    //set up timer B in capture mode (TBMR = 3, TBAMS = 0),
    //for edge time (TBCMR = 1) and up-counting (TBCDIR = 1)
    HWREG(WTIMER2_BASE+TIMER_O_TBMR) = 
        (HWREG(WTIMER2_BASE+TIMER_O_TBMR) & ~TIMER_TBMR_TBAMS) |
            (TIMER_TBMR_TBCDIR | TIMER_TBMR_TBCMR | TIMER_TBMR_TBMR_CAP);
    
    //set event to on rising edge
    HWREG(WTIMER2_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEVENT_M;
    
    //now set up port to do capture
    //set alternate function on pin PD1
    HWREG(GPIO_PORTD_BASE+GPIO_O_AFSEL) |= BIT1HI;
    
    HWREG(GPIO_PORTD_BASE+GPIO_O_PCTL) =
        (HWREG(GPIO_PORTD_BASE+GPIO_O_PCTL) & 0xffffff0f) + (7<<(1*BITS_PER_NIBBLE));
    
    //enable pin on port c for digital IO
    HWREG(GPIO_PORTD_BASE+GPIO_O_DEN) |= BIT1HI;
    //enable pin on port c as input
    HWREG(GPIO_PORTD_BASE+GPIO_O_DIR) &= BIT1LO;
    
    //enable local capture interrupt on the timer
    HWREG(WTIMER2_BASE+TIMER_O_IMR) |= TIMER_IMR_CBEIM;
    
    //enable timer B in wide timer 0 to interrup in the NVIC
    //interrupt #98, BIT3 on EN3
    HWREG(NVIC_EN3) |= BIT3HI;
    
    //make sure interrupts are enable globally
    __enable_irq();
    
    //kick off the timer by enabling it and enable timer to stop while stalled in the debugger
    HWREG(WTIMER2_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}

