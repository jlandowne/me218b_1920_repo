/****************************************************************************
 Module
   MinerService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "MinerService.h"
#include "FieldGameService.h"
#include "Field_RXSM.h"
#include "LEDService.h"

/*----------------------------- Module Defines ----------------------------*/

#define NUM_COLORS 16
#define HALF_SEC   500

#define RED 0
#define BLUE 1

#define MINER_1_MASK 0xF0
#define MINER_2_MASK 0x0F

#define COMPANY_MASK 0x80
#define MINER_SELECT_MASK 0x01

#define LG_MASK BIT7HI //Location Good
#define WFLG_MASK BIT6HI //White detection
#define UFLG_MASK BIT4HI //Unknown location

#define MINER_DEBUG_FLAG  1
#define MINER_DEBUG if(MINER_DEBUG_FLAG) printf
#define MINER_TO_TEST RED_2
	
#define MAX_VAL 255

// Default tolerance values
#define ERROR_H 6
#define ERROR_S 5
#define ERROR_V 4
#define H 0
#define S 1
#define V 2
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

typedef struct ColorSquare {
	uint8_t SquareNumber;
	uint16_t Hue;
	uint16_t Saturation;
	uint16_t Value;
} ColorSquare;

//Miner service internal functions
static void UpdateScore(void);
static void CategorizeMiner(void);
void Sensor2HSV(minerStruct Miner, uint16_t *ptrHSV);
bool containsOtherMiners(uint8_t square);
static ColorSquare ObtainSquare(uint8_t SquareNo, uint8_t MinerNo);
static void AppendFlag(uint8_t* val, uint8_t flag);
static void StoreMiner (uint8_t MinerFlag, uint8_t SquareNumber, uint8_t Flag);

//Last scoring position save
static uint8_t ScoreRed1Pos =0;
static uint8_t ScoreRed2Pos =0;
static uint8_t ScoreBlu1Pos =0;
static uint8_t ScoreBlu2Pos =0;


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

static uint16_t RedTeamScore = 0;
static uint16_t BlueTeamScore = 0;

/* Miner location is saved as follows:
MSB for Miner 2, LSB for Miner 1, 0 is square 1, 15 is square 16 */
static uint8_t RedMiner1Location = 0;
static uint8_t RedMiner2Location = 0;
static uint8_t BlueMiner1Location = 0;
static uint8_t BlueMiner2Location = 0;

static minerStruct lastMiner;
	

// Tolerances for each color
static const uint8_t Tol[NUM_COLORS][3] = {
	{ERROR_H, ERROR_S, ERROR_V},    	//COLOR 1
	{ERROR_H, ERROR_S, ERROR_V},		 	//COLOR 2
	{ERROR_H, ERROR_S, ERROR_V},		 		//COLOR 3
	{7, ERROR_S, ERROR_V},    		//COLOR 4 MINT
	{4, 3, 3},   	 	//COLOR 5	OLIVE
	{4, 3, 2},    		//COLOR 6 BEIGE
	{ERROR_H, ERROR_S, ERROR_V},    	//COLOR 7
	{ERROR_H, ERROR_S, ERROR_V},		 //COLOR 8
	{ERROR_H, ERROR_S, ERROR_V},    //COLOR 9
	{ERROR_H, ERROR_S, ERROR_V},	 				//COLOR 10
	{11, ERROR_S, ERROR_V},   		//COLOR 11 PURPLE
	{ERROR_H, ERROR_S, ERROR_V},   			//COLOR 12
	{60, 5, 4},   			//COLOR 13 NAVY
	{ERROR_H, ERROR_S, ERROR_V},   			//COLOR 14
	{ERROR_H, ERROR_S, ERROR_V},   		//COLOR 15
	{ERROR_H, ERROR_S, ERROR_V}};		//COLOR 16	

//Declare struct array to store each square color's thresholds
static const ColorSquare Squares[NUM_COLORS] = {
	{Lavender, 347, 7, 33},    	//COLOR 1
	{Maroon, 11, 44, 44},		 	//COLOR 2
	{Blue, 200, 45, 41},		 		//COLOR 3
	{Mint, 90, 26, 37},    		//COLOR 4 
	{Olive, 46, 33, 37},   	 	//COLOR 5	
	{Beige, 51, 30, 36},    		//COLOR 6
	{Orange, 13, 64, 52},    	//COLOR 7
	{Magenta, 347, 39, 42},		 //COLOR 8
	{Apricot, 30, 40, 40},    //COLOR 9
	{Red, 4, 66, 57},	 				//COLOR 10
	{Purple, 314, 23, 35},   		//COLOR 11
	{Cyan, 170, 34, 37},   			//COLOR 12
	{Navy, 234, 11, 35},   			//COLOR 13
	{Lime, 57, 50, 38},   			//COLOR 14
	{Brown, 22, 50, 44},   		//COLOR 15
	{Yellow, 40, 60, 43}};		//COLOR 16	

	//Declare struct array to store each square color's thresholds
static const ColorSquare Blue1[NUM_COLORS+1] = {
	{Lavender, 351, 7, 34},    	//COLOR 1
	{Maroon, 9, 46, 47},		 	//COLOR 2
	{Blue, 200, 39, 42},		 		//COLOR 3
	{Mint, 90, 26, 38},    		//COLOR 4 
	{Olive, 45, 37, 39},   	 	//COLOR 5	
	{Beige, 52, 30, 37},    		//COLOR 6
	{Orange, 12, 65, 55},    	//COLOR 7
	{Magenta, 350, 39, 44},		 //COLOR 8
	{Apricot, 28, 41, 41},    //COLOR 9
	{Red, 3, 69, 62},	 				//COLOR 10
	{Purple, 335, 23, 38},   		//COLOR 11
	{Cyan, 169, 33, 38},   			//COLOR 12
	{Navy, 270, 6, 36},   			//COLOR 13
	{Lime, 54, 51, 40},   			//COLOR 14
	{Brown, 19, 52, 47},   		//COLOR 15
	{Yellow, 38, 60, 45}, 		//COLOR 16
  { White, 50, 20, 35} };	

		//Declare struct array to store each square color's thresholds
static const ColorSquare Blue2[NUM_COLORS+1] = {
	{Lavender, 348, 11, 34},    	//COLOR 1
	{Maroon, 9, 44, 46},		 	//COLOR 2
	{Blue, 202, 41, 40},		 		//COLOR 3
	{Mint, 86, 26, 37},    		//COLOR 4 
	{Olive, 47,37,38},   	 	//COLOR 5	
	{Beige, 48,30,37},    		//COLOR 6
	{Orange, 11,65,54},    	//COLOR 7
	{Magenta, 349,42,44},		 //COLOR 8
	{Apricot, 26,40,41},    //COLOR 9
	{Red, 3,67,60},	 				//COLOR 10
	{Purple, 321,25,38},   		//COLOR 11
	{Cyan, 168,32,38},   			//COLOR 12
	{Navy, 235,5,35},   			//COLOR 13
	{Lime, 55,49,40},   			//COLOR 14
	{Brown, 19,50,46},   		//COLOR 15
	{Yellow, 37,59,45}, 	//COLOR 16
  { White, 53, 20, 34} };	
static const ColorSquare Red1[NUM_COLORS+1] = {
	{	Lavender,	334, 8, 33},
	{	Maroon,	8, 44, 45},
	{	Blue,	205, 44, 41},
	{	Mint,	92, 25, 36},
	{	Olive,	46, 36, 37},
	{	Beige,	52, 29, 36},
	{	Orange,	12, 64, 53},
	{	Magenta,	345, 40, 42},
	{	Apricot,	27, 40, 41},
	{	Red,	4, 67, 58},
	{	Purple,	312, 23, 36},
	{	Cyan,	174, 32, 36},
	{	Navy,	245, 11, 35},
	{	Lime,	56, 49, 38},
	{	Brown,	20, 49, 44},
	{	Yellow,	38, 59, 43},
  { White, 54, 21, 34} };	
static const ColorSquare Red2[NUM_COLORS+1] = {
	{	Lavender	,	320, 3, 33	},
	{	Maroon	,	7, 40, 43	},
	{	Blue	,	202, 45, 41	},
	{	Mint	,	100, 25, 38	},
	{	Olive	,	55, 33, 39	},
	{	Beige	,	60, 26, 35	},
	{	Orange	,	13, 63, 53	},
	{	Magenta	,	347, 35, 42	},
	{	Apricot	,	32, 38, 40	},
	{	Red	,	5, 66, 58	},
	{	Purple	,	310, 19, 36	},
	{	Cyan	,	171, 36, 38	},
	{	Navy	,	225, 12, 36	},
	{	Lime	,	61, 50, 39	},
	{	Brown	,	23, 49, 45	},
	{	Yellow	,	41, 59, 43},
  { White, 61, 16, 43}};		
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMinerService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitMinerService(uint8_t Priority)
{
  ES_Event ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostMinerService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostMinerService(ES_Event ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMinerService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event RunMinerService(ES_Event ThisEvent)
{
  ES_Event ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************
   in here you write your service code
   *******************************************/
	//If received a start mining event
	if (ThisEvent.EventType == ES_GAME_START) {
		MINER_DEBUG("Starting game\n\r");
		//Reset score variables and miner positions
		RedMiner1Location = 0;
    RedMiner1Location = 0;
		RedTeamScore = 0;
		BlueMiner1Location = 0;
    BlueMiner2Location = 0;
		BlueTeamScore = 0;
		//Restore scoring pos
		ScoreRed1Pos =0;
		ScoreRed2Pos =0;
		ScoreBlu1Pos =0;
		ScoreBlu2Pos =0;
		//Start timer for scoring
		ES_Timer_InitTimer(SCORING_TIMER, COUNTDOWN_TIME +500);
		
	}
	else if(ThisEvent.EventType == ES_MINER_UPDATE) {
		#ifndef MINER_DEBUG
		lastMiner = GetLastMiner();
		CategorizeMiner();
		#else
		//RedMinersLocation = (RedMinersLocation + 1) % 16;
		lastMiner = GetLastMiner();
		//MINER_DEBUG("R: %4x,G: %4x, B: %4x Miner: %d\n\r",lastMiner.RedVal,lastMiner.GrnVal,
		//lastMiner.BluVal,lastMiner.MinerVal);
		CategorizeMiner();
		#endif
	}
	//If received an ES_TIMEOUT event
	else if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == SCORING_TIMER) {
		if (QueryFieldGameState() == PermitsIssued) {
			//Call update score function
			UpdateScore();
			ES_Timer_InitTimer(SCORING_TIMER, HALF_SEC);
		}
	}
	else if (ThisEvent.EventType == ES_SCORE_TEST) {
		//Call update score function
		if(ThisEvent.EventParam == RED) {
			RedTeamScore += 10;
		} else if(ThisEvent.EventParam == BLUE) {
			//MINER_DEBUG("Increase Score");
			BlueTeamScore += 10;
		}
		
	}
		
  return ReturnEvent;
}
/***************************************************************************
 Getter Functions
 ***************************************************************************/
uint8_t QueryC1MLoc1 (void) {
	return (RedMiner1Location);
}
uint8_t QueryC1MLoc2 (void) {
	return (RedMiner2Location);
}
uint8_t QueryC2MLoc1 (void) {
	return (BlueMiner1Location);
}
uint8_t QueryC2MLoc2 (void) {
	return (BlueMiner2Location);
}
uint8_t GetMinerLocation (uint8_t Miner) {
	switch (Miner) {
		case RED_1: {
			return RedMiner1Location & 0x0F;
		}
		case RED_2: {
      return RedMiner2Location & 0x0F;
		}
		case BLUE_1: {
			return BlueMiner1Location & 0x0F;
		}
		case BLUE_2: {
			return BlueMiner2Location & 0x0F;
		}
	}
	return 0; //Return 0 for test
}

uint16_t GetRedScore (void) {
		return RedTeamScore;
}

uint16_t GetBlueScore (void) {
		return BlueTeamScore;
}

uint16_t QueryC1Res(void) {
		return RedTeamScore;
}
uint16_t QueryC2Res(void) {
		return BlueTeamScore;
}

/***************************************************************************
 Setter functions
 ***************************************************************************/
void ResetScores (void) {
		BlueTeamScore = 0;
		RedTeamScore = 0;
}
	
/***************************************************************************
 private functions
 ***************************************************************************/
static void UpdateScore (void) {
	ES_Event PostEvent;
	//Call field game state to obtain current valid score
	ScoringLocations ScoreLocs = GetScoringLocations();


	for (int i = RED_1; i <= BLUE_2; i ++) {
		//Call field game state to obtain current valid score
		uint8_t MinerToCheck = GetMinerLocation(i);
    
		if (i == RED_1 || i == RED_2) {
			//Check if valid scoring location
			if ((MinerToCheck == ScoreLocs.RedLoc || MinerToCheck == ScoreLocs.Neut1Loc || MinerToCheck == ScoreLocs.Neut2Loc) && !containsOtherMiners(MinerToCheck)) {
					RedTeamScore ++;
					if (i == RED_1 && MinerToCheck != ScoreRed1Pos) {
						PostEvent.EventType = ES_SCORED;
						PostEvent.EventParam = MinerToCheck;
						PostLEDService(PostEvent);
						//Update new position
						ScoreRed1Pos = MinerToCheck;
					} else if (i == RED_2 && MinerToCheck != ScoreRed2Pos) {
						PostEvent.EventType = ES_SCORED;
						PostEvent.EventParam = MinerToCheck;
						PostLEDService(PostEvent);
						//Update new position
						ScoreRed2Pos = MinerToCheck;
					}
			}
		}
		else if (i == BLUE_1 || i == BLUE_2) {
			if ((MinerToCheck == ScoreLocs.BluLoc || MinerToCheck == ScoreLocs.Neut1Loc || MinerToCheck == ScoreLocs.Neut2Loc) && !containsOtherMiners(MinerToCheck)) {
					BlueTeamScore ++;
					if (i == BLUE_1 && MinerToCheck != ScoreBlu1Pos) {
							PostEvent.EventType = ES_SCORED;
							PostEvent.EventParam = MinerToCheck;
							PostLEDService(PostEvent);
							//Update new position
							ScoreBlu1Pos = MinerToCheck;
						} else if (i == BLUE_2 && MinerToCheck != ScoreBlu2Pos) {
							PostEvent.EventType = ES_SCORED;
							PostEvent.EventParam = MinerToCheck;
							PostLEDService(PostEvent);
							//Update new position
							ScoreBlu2Pos = MinerToCheck;
						}
				}
			}
		}
}


static void StoreMiner (uint8_t MinerFlag, uint8_t SquareNumber, uint8_t Flag) {
	switch (MinerFlag) {
		case RED_1 : {
			RedMiner1Location = (SquareNumber);
      AppendFlag(&RedMiner1Location, Flag);
		} 
		break;
		case RED_2 : {
			//Clear Miner 2 location, and update the value
			RedMiner2Location = (SquareNumber);
      AppendFlag(&RedMiner2Location, Flag);
		} 
		break;
		case BLUE_1 : {
			//Clear Miner 1 location, and update the value
			BlueMiner1Location = (SquareNumber);
      AppendFlag(&BlueMiner1Location, Flag);
		}
		break;
		case BLUE_2 : {
			//Clear Miner 2 location, and update the value
			BlueMiner2Location = (SquareNumber);
      AppendFlag(&BlueMiner2Location, Flag);
		}
	}
}

static void AppendFlag(uint8_t* val, uint8_t flag) {
    *val &= 0x0F; //Clear previous flags
    switch(flag) { //Mask in flag
      case LG_MASK:
        *val |= LG_MASK;
      break;
      case WFLG_MASK:
        *val |= WFLG_MASK;
      break;
      case UFLG_MASK:
        *val |= UFLG_MASK;
      break;
    }
}
  
			
/* TODO: CHANGE FOR HSV CALCS */
static void CategorizeMiner(void) {
	ColorSquare thisSquare;
	uint16_t MinerHSV [3];
	Sensor2HSV(lastMiner, MinerHSV);
	for (uint8_t i = 0; i < NUM_COLORS; i++) { //Check for the 16 colors
			thisSquare = ObtainSquare(i,lastMiner.MinerVal);
			if((MinerHSV[0] <= thisSquare.Hue + Tol[i][H]) &&
					(MinerHSV[0] >= thisSquare.Hue - Tol[i][H]) &&
					(MinerHSV[1] <= thisSquare.Saturation + Tol[i][S]) &&
					(MinerHSV[1] >= thisSquare.Saturation - Tol[i][S]) &&
					(MinerHSV[2] <= thisSquare.Value + Tol[i][V]) &&
					(MinerHSV[2] >= thisSquare.Value - Tol[i][V])) {
						//Store miner color 
						//MINER_DEBUG("M%d @ square %d\n\r",lastMiner.MinerVal,thisSquare.SquareNumber);
						StoreMiner(lastMiner.MinerVal, thisSquare.SquareNumber, LG_MASK); //Store good color
						//End function
						return;
			} 
		}
     thisSquare = ObtainSquare(16,lastMiner.MinerVal); //Check for white
			if((MinerHSV[0] <= thisSquare.Hue + ERROR_H) &&
					(MinerHSV[0] >= thisSquare.Hue - ERROR_H) &&
					(MinerHSV[1] <= thisSquare.Saturation + ERROR_S) &&
					(MinerHSV[1] >= thisSquare.Saturation - ERROR_S) &&
					(MinerHSV[2] <= thisSquare.Value + ERROR_V) &&
					(MinerHSV[2] >= thisSquare.Value - ERROR_V)) {
						//Store miner color 
						StoreMiner(lastMiner.MinerVal, thisSquare.SquareNumber, WFLG_MASK); //Store white color
						//End function
						return;
			}
  StoreMiner(lastMiner.MinerVal, 0, UFLG_MASK); // Unrecognizable color 
}

static ColorSquare ObtainSquare(uint8_t SquareNo, uint8_t MinerNo) {
	switch (MinerNo) {
		case RED_1: {
			return Red1[SquareNo];
		}
		case RED_2: {
			return Red2[SquareNo];
		}
		case BLUE_1: {
			return Blue1[SquareNo];
		}
		case BLUE_2: {
			return Blue2[SquareNo];
		}
	}
  return Squares[SquareNo];
}

 /****************************************************************************
 Function
    Sensor2HSV

 Parameters
   Miner struct received from RXSM
	 Empty array of HSV values that will be populated by the function

 Returns
   None

 Description
   Transforms the sensor reading into a HSV color
 Notes

 Author
   Pablo Martinez, with code based on Dimitri Petrakis' code
****************************************************************************/
 void Sensor2HSV(minerStruct Miner, uint16_t *ptrHSV)
 { // h, s, v = hue, saturation, value 
	 //Create local array to store RGB values
	 
	 uint16_t ptrRGB255[3];
	 // Convert the sensor element to a standard RGB value 0-255
	 ptrRGB255[0] = (uint16_t)((Miner.RedVal*MAX_VAL)/Miner.ClrVal);
   ptrRGB255[1] = (uint16_t)((Miner.GrnVal*MAX_VAL)/Miner.ClrVal);
   ptrRGB255[2] = (uint16_t)((Miner.BluVal*MAX_VAL)/Miner.ClrVal);

   // Save values as floats
   float R = ptrRGB255[0]; 
   float G = ptrRGB255[1];
   float B = ptrRGB255[2];
   
   // normalizes from 0-255 to 0-1
   R = R/MAX_VAL;
   G = G/MAX_VAL;
   B = B/MAX_VAL;
   
   float RGBNorm[3] = {R, G, B};
   // Function to get the max of the RGB values
   float Cmax = 0;
   for(int i = 0; i < 3; i++)
   {
     Cmax = (RGBNorm[i] > Cmax) ? RGBNorm[i]:Cmax;
   }
   // Function to get the min of the RGB values
   float Cmin = 10000;
   for(int i = 0; i < 3; i++)
   {
     Cmin = (RGBNorm[i] < Cmin) ? RGBNorm[i]:Cmin;
   }
   float Diff = Cmax - Cmin; // diff of cmax and cmin. 
   // if cmax and cmax are equal then h = 0 
        if (Cmax == Cmin) 
        {
          ptrHSV[0] = 0; 
        }
        // if cmax equal r then compute h 
        else if (Cmax == R) 
        {
          ptrHSV[0] = (uint16_t)(60 * ((G - B) / Diff) + 360) % 360; 
        }
        // if cmax equal g then compute h 
        else if (Cmax == G) 
        {
          ptrHSV[0] = (uint16_t)(60 * ((B - R) / Diff) + 120) % 360; 
        }  
        // if cmax equal b then compute h 
        else if (Cmax == B) 
        {
          ptrHSV[0] = (uint16_t)(60 * ((R - G) / Diff) + 240) % 360; 
        }
        else 
        {
          printf("Bro\n\r");
        }
        // if cmax equal zero 
        if (Cmax == 0) 
        {
          ptrHSV[1]= 0; 
        }
        else
        {
          ptrHSV[1] = (Diff / Cmax) * 100; 
        }
        // compute v 
        ptrHSV[2]= Cmax * 100; 
        //if(Miner.MinerVal == MINER_TO_TEST)
          //MINER_DEBUG("Miner: %d -- %d, %d, %d\n\r", Miner.MinerVal, ptrHSV[0],ptrHSV[1],ptrHSV[2]);     
   return;     
 }
 
 bool containsOtherMiners(uint8_t square) {
    uint8_t count = 0;
	 for (uint8_t miner = 0; miner < 4; miner++) {
		 if(GetMinerLocation(miner) == square) {
        count++;
     }
   }
	 return count >= 2;
 }

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

