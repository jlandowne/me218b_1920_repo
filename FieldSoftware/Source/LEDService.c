/****************************************************************************
 Module
   LED_Service.c

 Revision
   1.0.1

 Description
   This is a service designed to drive the SK9822 LED strips for the ME218B
   Winter 2020 game field

   It can write to an arbitrary number of LED strips, each with an arbitrary
   number of LEDs

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02-19-2020     A Zhang Initial Release
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// This module
#include "LEDService.h"

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"

// Hardware
#include "inc/hw_pwm.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_nvic.h"
#include "inc/hw_ssi.h"
#include "inc/hw_Timer.h"
#include "FieldGameService.h"

/*----------------------------- Module Defines ----------------------------*/
//#define LED_TESTING

#define NUM_SCORE_ZONES   4

#define ALL_BITS (0xff<<2)

#define LED_NUM_STRIPS    3
#define LED_GRID_SIZE     4
#define LED_PER_EDGE      18
#define LED_NUM_EDGES     4
#define LED_PER_STRIP     74

#define X_STRIP           0
#define Y_STRIP           1

#define LED_START_FRAME   (uint32_t) 0
#define LED_STOP_FRAME    ~LED_START_FRAME
#define LED_STOP_LEN			20
#define LED_FRAME_TEMP		0xE0000000
#define LED_FRAME_BRIGHT	30
#define LED_FRAME_MASK    LED_FRAME_TEMP | (LED_FRAME_BRIGHT << 24)

#define BRIGHT_MIN        0
#define BRIGHT_MAX        255

#define BLUE_SHIFT        16
#define GREEN_SHIFT       8
#define RED_SHIFT         0

#define LED_UPDATE_DELAY	1000
#define LED_STROBE_DELAY	1200
#define LED_SCORE_DELAY		2000

// Color Code Definitions
#define RED               0x0000FF
#define GREEN             0x00FF00
#define BLUE              0xFF0000
#define WHITE							0xFFFFFF
#define YELLOW						0x00FFFF
#define MAGENTA						0xFF00FF
#define CYAN							0xFFFF00
#define OFF								0x000000

// Multiplexer Pins
#define MUX_PORT					GPIO_PORTB_BASE	

#define AXIS_SEL_OFFSET		2
#define STRIP_SEL_OFFSET	0

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

// Scoring Zone Functions
static void setBrightness(zone_t scoringZone, uint8_t newBrightness);
static void toggleBrightness(zone_t scoringZone);

// LED Strip/SPI Functions
static void initSPI(void);
static void write32Bits(uint32_t dataOut);

static void resetLEDStrips(void);
static void drawLEDLine(bool axis, uint8_t strip, uint8_t edge, uint32_t color);
static void drawLEDNumber(int number);
static void buildLEDData(void);

static void initMux(void);
static void selectLEDStrip(bool which, uint8_t index);
static void writeLEDStart(void);
static void writeLEDStrip(bool axis, uint8_t strip);
static void writeLEDStop(void);
static void testLEDStrip(uint32_t color);

// Gamma correction array

const uint8_t gamma[] = {
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
    1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
    2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
    5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
   10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
   17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
   25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
   37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
   51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
   69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
   90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
  115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
  144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
  177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
  215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255 };

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

// Initialize scoring zone data
zone_t scoringZones[] = {
	{BRIGHT_MAX, RED, 0 ,0, false},
	{BRIGHT_MAX, BLUE, 1 ,4, false},
	{BRIGHT_MAX, MAGENTA, 2 ,3, false},
	{BRIGHT_MAX, MAGENTA, 3 ,2, false}
};

static ScoringLocations zones;

// Initialize data to be written via SPI to LED strip
// The first index is the data stream to be written to each LED strip
// The second index is which LED along the axis to write to
static uint32_t ledStripX[LED_PER_STRIP][LED_GRID_SIZE + 1];
static uint32_t ledStripY[LED_PER_STRIP][LED_GRID_SIZE + 1];

// Buffer containing data to be written to the SPI Tx output.
static uint32_t dataOut[LED_PER_STRIP * 6];
static uint16_t dataLen = LED_PER_STRIP * 6;
static uint16_t dataIndex = 0;

static uint32_t dataTest[LED_PER_STRIP];

static bool strobing = false;

/*------------------------------  Module Code ------------------------------*/
/****************************************************************************
 Function
     InitLED_Service

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitLEDService(uint8_t Priority)
{
  ES_Event ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  	
	initSPI();
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostLED_Service

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostLEDService(ES_Event ThisEvent)
{ 
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunLED_Service

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event RunLEDService(ES_Event ThisEvent)
{
  ES_Event ReturnEvent;
	ES_Event PostEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  static bool currentAxis = X_STRIP;
  static uint8_t currentStrip = 1;
	
	static uint32_t testColor = WHITE;
	static int strobeCount = 0;
	static int startCounter = 3;
	
	//static ScoringLocations zones;
	
	static LEDState_t currentState = LED_Init;
	
	#ifndef LED_TESTING
	
	// Handle SPI interrupt events no matter what state we're in
	if (ThisEvent.EventType == LED_TX_COMPLETE) {
		// Start controlling the next strip
      currentStrip++;

      // If all strips along an axis have been written to, move to the next one
      if (currentStrip == LED_GRID_SIZE) {
        currentStrip = 1;
        currentAxis = !currentAxis;
      }

      // If we are not yet at our starting position, send another transmission
      if (!(currentAxis == X_STRIP && currentStrip == 1)) {
        writeLEDStrip(currentAxis, currentStrip);
      } else {
				writeLEDStop();
      }
	} else if (ThisEvent.EventType == LED_END_STROBE) {
		writeLEDStop();
	}
	// State machine for LEDs
	switch (currentState) {
		case LED_Init:
		if (ThisEvent.EventType == ES_INIT) {
			ES_Timer_InitTimer(LED_UPDATE_TIMER, LED_STROBE_DELAY);
			currentState = LED_Standby;
		}
		break;
		
		// Standby mode; this acts as a screen save for when the game is not active.  It simply cycles
		// through various colors with a snaking animation
		case LED_Standby:
		strobing = true;
		
		if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == LED_UPDATE_TIMER) {
			strobeCount = (strobeCount + 1) % 4;
			ES_Timer_InitTimer(LED_UPDATE_TIMER, LED_STROBE_DELAY);
			
			switch (strobeCount) {
				case 0:
					testLEDStrip(RED);
				break;
				
				case 1:
					testLEDStrip(BLUE);
				break;
				
				case 2:
					testLEDStrip(GREEN);
				break;
				
				case 3:
					testLEDStrip(MAGENTA);
			}
		} else if (ThisEvent.EventType == ES_GAME_START) {
			currentState = LED_Starting;
			startCounter = 3;
			ES_Timer_InitTimer(LED_UPDATE_TIMER, 1500);
			drawLEDNumber(startCounter);
			strobing = false;
		}
		break;
		
		case LED_Starting:
			if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == LED_UPDATE_TIMER) {
				
				if (startCounter == 0) {
					currentState = LED_Game;
					testLEDStrip(CYAN);
					ES_Timer_InitTimer(LED_UPDATE_TIMER, 1000);
					
					PostEvent.EventType = ES_LOCATION_UPDATE;
					PostLEDService(PostEvent);
				} else {
					drawLEDNumber(startCounter);
					startCounter--;
					ES_Timer_InitTimer(LED_UPDATE_TIMER, 2500);
				}
			}
		break;
		
		case LED_Game:
		if (ThisEvent.EventType == ES_SCORED) {
			// Query which region just scored
			int scoreRegion;
			
			// Set scoring to true
			scoringZones[scoreRegion].scoring = true;
			
			// Update 
			ES_Timer_InitTimer(LED_SCORE_TIMER, LED_SCORE_DELAY);
			PostEvent.EventType = LED_UPDATE;
			PostLEDService(PostEvent);			
		} 
		// Update LEDs by refreshing square locations and colors from scoring zone data
		else if (ThisEvent.EventType == LED_UPDATE) {
			buildLEDData();
			currentAxis = X_STRIP;
			currentStrip = 1;
		
			writeLEDStart();
      writeLEDStrip(currentAxis, currentStrip);
			
			ES_Timer_InitTimer(LED_UPDATE_TIMER, LED_UPDATE_DELAY);
		}
		else if (ThisEvent.EventType == ES_TIMEOUT) {
			// If a certain period has passed after a region scored, reset its color to the original
			if (ThisEvent.EventParam == LED_SCORE_TIMER) {
				for (int i = 0; i < NUM_SCORE_ZONES; i++) {
					scoringZones[i].scoring = false;
				}
				PostEvent.EventType = LED_UPDATE;
				PostLEDService(PostEvent);	
			}
			// Refresh the LED's based on scoring zone data each time this timer expires
			else if (ThisEvent.EventParam == LED_UPDATE_TIMER) {
				PostEvent.EventType = LED_UPDATE;
				PostLEDService(PostEvent);
			}
		}
		else if(ThisEvent.EventType == ES_LOCATION_UPDATE) {
			zones = GetScoringLocations();
			int locations[] = {zones.RedLoc, zones.BluLoc, zones.Neut1Loc, zones.Neut2Loc};
			//printf("\r\n");
			for (int i = 0; i < NUM_SCORE_ZONES; i++) {
				scoringZones[i].coordX = locations[i]/4;
				scoringZones[i].coordY = locations[i]%4;
				//printf("i: %d  loc: %d  X: %d  y %d \r\n",i, locations[i], scoringZones[i].coordX, scoringZones[i].coordY);
			}
		}
		else if(ThisEvent.EventType == ES_SCORED) {
			scoringZones[ThisEvent.EventParam].scoring = true;
		}
			
		else if (ThisEvent.EventType == ES_GAME_END || ThisEvent.EventType == ES_RESET) {
			ES_Timer_StopTimer(LED_SCORE_TIMER);
			ES_Timer_StopTimer(LED_UPDATE_TIMER);
			
			currentState = LED_Init;
			PostEvent.EventType = ES_INIT;
			PostLEDService(PostEvent);
		}
		
		break;
	}
	#endif

	#ifdef LED_TESTING
	// Test events for LEDs
  switch (ThisEvent.EventType)
  {
    case ES_INIT:
    {
      
    }
    break;
		
    case ES_NEW_KEY:
			switch (ThisEvent.EventParam) {
				ES_Event Event;
				
				case 'r':
				strobing = true;
        testLEDStrip(RED);
				break;
				
				case 'g':
				strobing = true;
        testLEDStrip(GREEN);
				break;
				
				case 'b':
				strobing = true;
        testLEDStrip(BLUE);
				break;
				
				case 'w':
				strobing = true;
        testLEDStrip(WHITE);
				break;
				
				case 'c':
				strobing = true;
        testLEDStrip(CYAN);
				break;
				
				case 'm':
				strobing = true;
        testLEDStrip(MAGENTA);
				break;
				
				case 'y':
				strobing = true;
        testLEDStrip(YELLOW);
				break;
				
				case 'o':
				strobing = true;
				testLEDStrip(OFF);
				break;
				
				case 't':
				Event.EventType = LED_UPDATE;
				strobing = false;
				PostLEDService(Event);
				break;
				
				case 'p':
				ES_Timer_InitTimer(LED_UPDATE_TIMER, 1000);
				strobing = true;
				break;
				
				case 's':
				ES_Timer_StopTimer(LED_UPDATE_TIMER);
				ES_Timer_StopTimer(LED_SCORE_TIMER);
				break;
				
				case '3':
				strobing = false;
				drawLEDNumber(3);
				break;
				
				case '2':
				strobing = false;
				drawLEDNumber(2);
				break;
				
				case 1:
				strobing = false;
				drawLEDNumber(1);
				break;
				
				default:
					
				break;
			}
	break;			

    /*
    This event only comes from the SPI Tx ISR.  It signals that it finished
    writing values to all LEDs in one strip.

    Note that each axis starts at 1 and ends before the number of grids + 1.  This 
    is because we have chosen not to fill in the borders of the game field with
    LEDs.  The software arrays still contain values for these borders to simplify
    the program and help avoid SEGFAULT errors, though.
    */
    case LED_TX_COMPLETE:
      // Start controlling the next strip
      currentStrip++;

      // If all strips along an axis have been written to, move to the next one
      if (currentStrip == LED_GRID_SIZE) {
        currentStrip = 1;
        currentAxis = !currentAxis;
      }

      // If we are not yet at our starting position, send another transmission
      if (!(currentAxis == X_STRIP && currentStrip == 1)) {
        writeLEDStrip(currentAxis, currentStrip);
      } else {
				writeLEDStop();
      }
    break;
			
		case LED_UPDATE:
			buildLEDData();
			currentAxis = X_STRIP;
			currentStrip = 1;
		
			writeLEDStart();
      writeLEDStrip(currentAxis, currentStrip);
		
		break;

    /*
    This event should be received from the game field whenever scoring zones are
    moved.  Depending on implementation, either the scoring zone data will be 
    sent to this service, or it will be queried from the main service.
    */
		
    case ES_LOCATION_UPDATE:
			zones = GetScoringLocations();
			int locations[] = {zones.RedLoc, zones.BluLoc, zones.Neut1Loc, zones.Neut2Loc};
			
			for (int i = 0; i < NUM_SCORE_ZONES; i++) {
				scoringZones[i].coordX = locations[i]%4;
				scoringZones[i].coordY = locations[i]/4;
			}
    break;

    /* This event is received from the game field whenever a zone starts or stops
    scoring for a team.
    */
		
    case ES_SCORED:
			scoringZones[ThisEvent.EventParam].scoring = true;
    break;
		
		
		case LED_END_STROBE:
			writeLEDStop();
		break;

    /*
    This internal timer serves as a blinking routine for scoring zones.  If a zone
    is currently scoring, it will flash on and off.
    */
    case ES_TIMEOUT:
      if (ThisEvent.EventParam == LED_SCORE_TIMER) {
        for (int i = 0; i < NUM_SCORE_ZONES; i++) {
          if (scoringZones[i].scoring == true) {
            // Toggle the brightness from 0 to 255 (0xFF) and back
            scoringZones[i].brightness ^= 0xFF;
          }
        }
      } else if (ThisEvent.EventParam == LED_UPDATE_TIMER) {
				strobeCount = (strobeCount + 1) % 4;
				ES_Timer_InitTimer(LED_UPDATE_TIMER, LED_STROBE_DELAY);
				
				switch (strobeCount) {
					case 0:
						testLEDStrip(RED);
					break;
					
					case 1:
						testLEDStrip(BLUE);
					break;
					
					case 2:
						testLEDStrip(GREEN);
					break;
					
					case 3:
						testLEDStrip(MAGENTA);
				}
			}
    break;

    default:
    {}
    break;
	}
	#endif
	
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
/***************************************************************************
Function
    resetLEDStrips

 Parameters
   void

 Returns
   void

 Description
   Fills all the LED data arrays with the empty frame (0xFF000000).  This
   prepares the arrays to be filled with color data from the scoring zones.

 Notes

 Author
   Alex Zhang
***************************************************************************/
static void resetLEDStrips(void) {
  for (uint8_t i = 0; i < LED_PER_STRIP; i++) {
    for (uint8_t j = 0; j < LED_GRID_SIZE + 1; j++) {
      ledStripX[i][j] = LED_FRAME_MASK;
      ledStripY[i][j] = LED_FRAME_MASK;
    }
  }
}

static void drawLEDLine(bool axis, uint8_t strip, uint8_t edge, uint32_t color) {
	uint32_t data = LED_FRAME_MASK | color;
	
	for (int i = 0; i < LED_PER_EDGE; i++) {
		if (axis == X_STRIP) {
			ledStripX[edge*LED_PER_EDGE + i][strip] |= data;
		} else {
			ledStripY[edge*LED_PER_EDGE + i][strip] |= data;
		}
	}
}

static void drawLEDNumber(int number) {
	resetLEDStrips();
	
	dataLen = LED_PER_STRIP * 6;
	dataIndex = 1;
	
	switch (number) {
		case 3:
		// Draw pattern
		drawLEDLine(X_STRIP, 1, 2, CYAN);
		drawLEDLine(X_STRIP, 2, 2, CYAN);
		drawLEDLine(X_STRIP, 3, 2, CYAN);
		drawLEDLine(Y_STRIP, 3, 1, CYAN);
		drawLEDLine(Y_STRIP, 3, 2, CYAN);
		
		// Start Data Transmission
		writeLEDStart();
		writeLEDStrip(X_STRIP, 1);
		
		// Enable EOT Interrupt
		HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
		
		break;
		
		case 2:
		// Draw pattern
		drawLEDLine(X_STRIP, 1, 2, CYAN);
		drawLEDLine(X_STRIP, 2, 2, CYAN);
		drawLEDLine(X_STRIP, 3, 2, CYAN);
		drawLEDLine(Y_STRIP, 3, 1, CYAN);
		drawLEDLine(Y_STRIP, 2, 2, CYAN);
		
		// Start Data Transmission
		writeLEDStart();
		writeLEDStrip(X_STRIP, 1);
		
		// Enable EOT Interrupt
		HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
		break;
		
		case 1:
		// Draw pattern
		drawLEDLine(Y_STRIP, 3, 1, CYAN);
		drawLEDLine(Y_STRIP, 3, 2, CYAN);
		
		// Start Data Transmission
		writeLEDStart();
		writeLEDStrip(X_STRIP, 1);
		
		// Enable EOT Interrupt
		HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
		break;
		
		case 0:
		testLEDStrip(CYAN);
		break;
	}
}

/***************************************************************************
 Function
    buildLEDData

 Parameters
   void

 Returns
   void

 Description
   Converts the data from the scoring zones (color, brightness, and location)
   and populates the data array for each LED strip.

   It draws a square around the (X,Y) coordinate of each scoring region, based
   on the color and brightness associated with each region.

 Notes

 Author
   Alex Zhang
***************************************************************************/
static void buildLEDData(void) {
  // Reset all LED Strips
  resetLEDStrips();

  // Use scoring zone data to light up corresponding LEDs
  for (uint8_t i = 0; i < NUM_SCORE_ZONES; i++) {
    // Combine brightness and color data into LED data
    uint32_t brightnessMask = (scoringZones[i].brightness << BLUE_SHIFT) +
        (scoringZones[i].brightness << GREEN_SHIFT) +
        (scoringZones[i].brightness << RED_SHIFT);

		uint32_t newValue;
    // Create new LED value from brightness and color values
		if (scoringZones[i].scoring == false) {
			newValue = brightnessMask & scoringZones[i].color;
		} else {
			newValue = brightnessMask & GREEN;
		}

    // Draw a square pattern centered around the coordinates of the scoring region
    for (uint8_t j = 0; j < LED_PER_EDGE; j++) {
      ledStripX[scoringZones[i].coordY*LED_PER_EDGE + j][scoringZones[i].coordX] |=
          newValue;

      ledStripX[scoringZones[i].coordY*LED_PER_EDGE + j][scoringZones[i].coordX + 1] |=
          newValue;

      ledStripY[scoringZones[i].coordX*LED_PER_EDGE + j][scoringZones[i].coordY] |=
          newValue;

      ledStripY[scoringZones[i].coordX*LED_PER_EDGE + j][scoringZones[i].coordY + 1] |=
          newValue;
    }
  }
}

/***************************************************************************
 Function
    initSPI

 Parameters
   void

 Returns
   void

 Description
   Initializes the Tiva hardware to convert PA2-5 into SPI pins.

   The CLK pin is switched to each LED strip via a digital multiplexer,
   while the MOSI controls the LED states.  MISO and SS are unused.

 Notes

 Author
   Alex Zhang
***************************************************************************/
static void initSPI(void) {
  //Activates clock to GPIO PortA
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R0;

  // Enables clock to SSI module 0
  HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R0;

  // Waits for GPIO port to be ready
  while ((HWREG(SYSCTL_PRGPIO) & BIT0HI) != BIT0HI)
  {}

  // Programs GPIO for alternate functions
  HWREG(GPIO_PORTA_BASE + GPIO_O_AFSEL) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);

  // PA2=SSIOClk, PA3=SSIOFss, PA4=SSIORx, PA5 SSIOTx
  // Writes in GPIOCTL to select desired functions
  HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) =
      (HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) & 0xff0000ff) + (2 << 4 * 2)
      + (2 << 4 * 3) + (2 << 4 * 4) + (2 << 4 * 5);

  // Programs PA2 to PA5 for digital I/O
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);

  // Programs PA2 to PA4 data directions
  // (PA2, PA3 and PA5 are outputs, PA4 is input)
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (BIT2HI | BIT3HI | BIT5HI);
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= (BIT4LO);
		
	// Programs Pull-up for the clock line for SPI mode 3
  HWREG(GPIO_PORTA_BASE + GPIO_O_PUR) |= (BIT2HI);

  //Waits for SSI0 to be ready
  while ((HWREG(SYSCTL_RCGCSSI) & SYSCTL_RCGCSSI_R0) != SYSCTL_RCGCSSI_R0)
  {}

  // Disables SSI0 0
  HWREG(SSI0_BASE + SSI_O_CR1) &= (~SSI_CR1_SSE);

  // Select Master mode, and TXRIS for End of Transmission
  HWREG(SSI0_BASE + SSI_O_CR1) &= (~SSI_CR1_MS);
  HWREG(SSI0_BASE + SSI_O_CR1) |= (SSI_CR1_EOT);

  // Configures SSI clock source to be the system clock
  HWREG(SSI0_BASE + SSI_O_CC) = (SSI_CC_CS_SYSPLL);

  // Configures clock prescaler (Bit Rate 15 kHz) CPSR = 132
  //HWREG(SSI0_BASE + SSI_O_CPSR) = HWREG(SSI0_BASE + SSI_O_CPSR) & (0xFFFFFF00) + 132;
	HWREG(SSI0_BASE + SSI_O_CPSR) = 240;
  // Set SCR to 19
  //HWREG(SSI0_BASE + SSI_O_CR0) = ((HWREG(SSI0_BASE + SSI_O_CR0) + (19 << SSI_CR0_SCR_S)));
	HWREG(SSI0_BASE + SSI_O_CR0) = 10 << 8;

  // Configure SPH = 1 because data is captured at the second edge
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPH;

  // Configure SPO = 0 because idle state is low
  HWREG(SSI0_BASE + SSI_O_CR0) &= ~SSI_CR0_SPO;

  // Freescale SPI Mode
  HWREG(SSI0_BASE + SSI_O_CR0) = ((HWREG(SSI0_BASE + SSI_O_CR0) &
      (~SSI_CR0_FRF_M)) | SSI_CR0_FRF_MOTO);

  // Sending 8 bit data between command generator & TIVA
  HWREG(SSI0_BASE + SSI_O_CR0) = ((HWREG(SSI0_BASE + SSI_O_CR0) &
      (~SSI_CR0_DSS_M)) | SSI_CR0_DSS_8);

  // Locally enable interrupts (TXIM in SSIIM)
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;

  // Enable SSI
  HWREG(SSI0_BASE + SSI_O_CR1) |= (SSI_CR1_SSE);

  // Enable NVIC bit 7 in EN0
  HWREG(NVIC_EN0) |= BIT7HI;

  // Globally enable interrupts
  __enable_irq(); // commented out because this is done in CommandFSM init
}

static void writeLEDStart(void) {
	// Write the start frame to the LED strips
  write32Bits(LED_START_FRAME);
}

/***************************************************************************
 Function
    writeLEDStrip

 Parameters
   bool axis, uint8_t strip

 Returns
   void

 Description
   Utilizes the SPI module and a multiplexed CLK line to write data to an
   LED strip.

 Notes

 Author
   Alex Zhang
***************************************************************************/
static void writeLEDStrip(bool axis, uint8_t strip) {
  // Set mux to select LED strip
	//selectLEDStrip(axis, strip);
	
	dataLen = LED_PER_STRIP;
	dataIndex = 1;
	
	for (int i = 0; i < LED_PER_STRIP; i++) {
		if (axis == X_STRIP) {
			// Account for second X-strip being backwards
			if (strip == 2) {
				dataOut[i] = ledStripX[LED_PER_STRIP - i][strip];
			} else {
				dataOut[i] = ledStripX[i][strip];
			}
		} else {
			// Account for first and third Y-strip being backwards
			if (strip == 1 || strip == 3) {
				dataOut[i] = ledStripY[LED_PER_STRIP - i][LED_GRID_SIZE - strip];
			} else {
				dataOut[i] = ledStripY[i][LED_GRID_SIZE - strip];
			}
		}
	}
	
	// Write the start frame to the LED strips
  write32Bits(dataOut[0]);

  // Enable EOT Interrupt
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
}

static void writeLEDStop(void) {
	dataLen = LED_STOP_LEN;
	dataIndex = 0;
	
	for (int i = 0; i < LED_STOP_LEN; i++) {
		dataOut[i] = LED_STOP_FRAME;
	}
	
	// Begin the transmission and let the Tx ISR handle the rest of the data.
  write32Bits(LED_STOP_FRAME);

  // Enable EOT Interrupt
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
}

/***************************************************************************
 Function
    write32Bits

 Parameters
   uint32_t dataOut

 Returns
   void

 Description
   Breaks a 32 bit number down into byte-sized chunks and sends them to 
	 the SPI output

 Notes

 Author
   Alex Zhang
***************************************************************************/
static void write32Bits(uint32_t dataOut) {
//printf("%x ", dataOut);
	
	for (int8_t i = 3; i >= 0; i--) {
		// Break the 32 bit number into 8 bit chunks, starting from MSB
		HWREG(SSI0_BASE + SSI_O_DR) = (dataOut >> 8 * i) & 0xFF;
	}
	
	//puts("\n\r");
}

/****************************************************************************
 Function
    ssiTxISR

 Parameters
   void

 Returns
   void

 Description
   Interrupt Service Routine for sending more data after completion of 
   previous transmission.

 Notes

 Author
   Alex Zhang
****************************************************************************/
void ssiTxISR(void) {
  // Write next set of data to the output buffer and increment index
  if (dataIndex < dataLen) {
    // Write next LED value to strip and increment index
    write32Bits(dataOut[dataIndex]);
    dataIndex++;
  } else {
    // Disables Local interrupts
    HWREG(SSI0_BASE + SSI_O_IM) &= (~SSI_IM_TXIM);
    
    // Post event to LEDService
		ES_Event Event;
		if (strobing == false && dataLen != LED_STOP_LEN) {
			Event.EventType = LED_TX_COMPLETE;
		} else if (strobing == true && dataLen != LED_STOP_LEN) {
			Event.EventType = LED_END_STROBE;
		}
		
    PostLEDService(Event);
  }
}

/***************************************************************************
 Function
    testLEDStrip

 Parameters
   void

 Returns
   void

 Description
   Test function for verifying the functionality of the LED strips

 Notes

 Author
   Alex Zhang
***************************************************************************/
static void testLEDStrip(uint32_t color) {
	static uint8_t brightness = 255;
	
	uint8_t brightGamma = gamma[brightness];
	
	uint32_t brightMask = (brightGamma << 16) + (brightGamma << 8) + brightGamma;
	
  uint32_t data = LED_FRAME_MASK | (color & brightMask);

  // Load color data into output buffer
  for (int i = 0; i < LED_PER_STRIP * 6; i++) {
    dataOut[i] = data;
  }

  dataLen = LED_PER_STRIP * 6;
  dataIndex = 1;

  // Begin the transmission and let the Tx ISR handle the rest of the data.
	writeLEDStart();
  write32Bits(dataOut[0]);

  // Enable EOT Interrupt
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;
}

static void selectLEDStrip(bool axis, uint8_t strip) {
	// Select X axis by default
	HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) &= BIT2LO;
	
	// If axis is Y_STRIP, select Y strip instead
	if (axis == Y_STRIP) {
		HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= BIT2HI;
	}
	
	// Write to the strip designated
	HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) &= BIT0LO & BIT1LO;
	HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) += strip;
}

static void initMux(void) {
	//Activates clock to GPIO PortB
  HWREG(SYSCTL_RCGCGPIO) |= BIT1HI;

  // Waits for GPIO port to be ready
  while ((HWREG(SYSCTL_PRGPIO) & BIT1HI) != BIT1HI)
  {}
		
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT2HI);
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT0HI | BIT1HI | BIT2HI);
		
	HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) &= BIT0LO & BIT1LO & BIT2LO;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

