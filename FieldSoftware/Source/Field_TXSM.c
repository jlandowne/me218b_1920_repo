/****************************************************************************
 Module
   Field_TXSM.c

 Revision
   1.0.1

 Description
   Field Transmission Service
 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
 02/03/18       hr       edits for 2018 b field, comments to legacy code
 ****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
 */
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "Field_TXSM.h"
#include "sci.h"
#include "FieldGameService.h"
#include "MinerService.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"  // Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "inc/hw_uart.h"
#include "inc/hw_nvic.h"

/*----------------------------- Module Defines ----------------------------*/

#define TX_DEBUG_FLAG  0
#define TX_DEBUG if(TX_DEBUG_FLAG) printf

#define START_DELIMIER_OFFSET 				0x00
#define LENGTH_MSB_OFFSET 						0x01
#define LENGTH_LSB_OFFSET 						0x02
#define FRAME_TYPE_OFFSET 						0x03
#define FRAME_ID_OFFSET 							0x04
#define DESTINATION_MSB_OFFSET 				0x05
#define DESTINATION_LSB_OFFSET 				0x06
#define OPTIONS_OFFSET 								0x07
#define DATA_OFFSET 									0x08
#define CHECKSUM_OFFSET 							XBEE_TXPACKET_MAX_LENGTH - 1

#define FRAME_ID 											0x01 	//Arbitrary value
#define FRAME_DATA_MANDATORY_LENGTH 	5 		//API Idendifier + Address (MSB/LSB) + RSSI + Options
#define PACKET_DATA_LENGTH 						11 		//Addresses 0x00 -> 0x0A
#define LENGTH_MSB 										0x00
#define LENGTH_LSB 										(FRAME_DATA_MANDATORY_LENGTH + PACKET_DATA_LENGTH)
#define XBEE_TXPACKET_MAX_LENGTH  		(LENGTH_LSB + 4) 	// Message Length + Start Delimiter + Length LSB/MSB + Checksum
#define BROADCAST_ADDRESS 						0xFF
#define OPTIONS 											0x00

#define START_DELIMITER 	0x7E
#define API_RX_DATA 			0x81
#define API_TX_STAT 			0x89
#define API_TX_REQ 				0x01
#define VALID_CKSUM 			0xFF

//Addresses of registers for data to send
#define STATUS_ADDRESS 		0x00
#define C1MLOC1_ADDRESS 	0x01
#define C1MLOC2_ADDRESS 	0x02
#define C2MLOC1_ADDRESS 	0x03
#define C2MLOC2_ADDRESS 	0x04
#define C1RESH_ADDRESS		0x05
#define C1RESL_ADDRESS		0x06
#define C2RESH_ADDRESS		0x07
#define C2RESL_ADDRESS		0x08
#define PUR1_ADDRESS 			0x09
#define PUR2_ADDRESS 			0x0A


#define STATUS_UPDATE_PERIOD 200       // milliseconds
#define STATUS_SLOW_UPDATE_PERIOD 1000       // milliseconds
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
 */

static void ConstructBroadcastMsg (void);
static int8_t SixteenToEightLSB(int16_t Byte);
static int8_t SixteenToEightMSB(int16_t Byte);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

static Field_TX_State_t currentState;

static uint8_t XBeePacket[XBEE_TXPACKET_MAX_LENGTH]; // Store the packet w/ some history
static uint8_t XBeePacketLen = sizeof(XBeePacket);
static uint8_t XBeeTXBuffer[XBEE_TXPACKET_MAX_LENGTH];   // Actually send this packet
static volatile uint8_t bufferLen;
static uint8_t bytesSent; 

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 Function
     InitFieldTXService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
 ****************************************************************************/
bool InitFieldTXService(uint8_t Priority) {
 
    MyPriority = Priority;
    //reset variables 
    bufferLen = 0;
    bytesSent = 0;

    currentState = Wait2Transmit; //set current state to wit 2 transmit
    
    ES_Timer_InitTimer(STATUS_TIMER, STATUS_UPDATE_PERIOD); //start timer to broadcast message
   
    return true;
}
/****************************************************************************
 Function
     PostFieldTXService

 Parameters
     ES_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue. First in First out.
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
 ****************************************************************************/
bool PostFieldTXService(ES_Event ThisEvent) {
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
     PostLIFOFieldTXService

 Parameters
     ES_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue. Last in first out.
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
 ****************************************************************************/
bool PostLIFOFieldTXService(ES_Event ThisEvent) {
    return ES_PostToServiceLIFO(MyPriority, ThisEvent);
}
/****************************************************************************
 Function
    RunFieldTXService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
  KJ Changes: changed buffer packet from 0 to 8
              increase status update timer to be slow

 Author
   J. Edward Carryer, 01/15/12, 15:23
 ****************************************************************************/
ES_Event RunFieldTXService(ES_Event ThisEvent) {
    ES_Event ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
 
    switch (currentState) {
        case Wait2Transmit: //if current state is wait2 transmit
        {
            if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == STATUS_TIMER) { //if we get a timeout from the status timer
                ConstructBroadcastMsg();
								TX_DEBUG("Packet: \r\n");    
                for(uint8_t i = 0; i < XBeePacketLen; i++){ //copy the packets into the buffer variable
                    XBeeTXBuffer[i] = XBeePacket[i];

                    TX_DEBUG("Byte %2.2x \r\n", XBeeTXBuffer[i]);        
                }
                //TX_DEBUG("TX: Bytes Sent prev: %d\n\r", bytesSent--);
                bytesSent = 0; //reset number of bytes sent
                bufferLen = XBeePacketLen;
                HWREG(UART3_BASE + UART_O_DR) = XBeeTXBuffer[bytesSent];   //Comment here for bench debugging
                bytesSent++; //increment bytes sent
             
                currentState = Transmitting; //change states to transmitting
            }
        }
        break;
        
        case Transmitting: //if current state is transmitting
        {
            if (ThisEvent.EventType == ES_TX_SEND_COMPLETE) {
                TX_DEBUG("TX: complete.\n\r");
                ES_Timer_InitTimer(STATUS_TIMER, STATUS_UPDATE_PERIOD); //start timer to broadcast message
                currentState = Wait2Transmit; //change state to wait2Transmit
            }
        }
            break;
        
        default:
            break;
    }
    return ReturnEvent;
}

/***************************************************************************
 Getters/Setters
 ***************************************************************************/

/****************************************************************************
 Function
     TX_getChecksum

 Parameters
   uint8_t fID

 Returns
    uint8_t 0xFF - checksum

 Description
   returns the check sum value
 Notes

 Author

 ****************************************************************************/
uint8_t TX_getChecksum() 
{
    uint8_t chk = 0; //reset check sum
    //adds all the values in the packet to get checksum
    for (uint8_t i = FRAME_TYPE_OFFSET; i < CHECKSUM_OFFSET; i++) 
    {
        chk += XBeePacket[i];
    }
    return 0xFF - chk;
}

/****************************************************************************
 Function
    TX_getXBeePacket

 Parameters
   uint8_t fID, uint8_t _index

 Returns
    uint8_t XBeePacket[fID][_index]

 Description
   add your description here
 Notes

 Author

 ****************************************************************************/
uint8_t TX_getXBeePacket(uint8_t _index) 
{
    return XBeePacket[_index];
}

/****************************************************************************
 Function
    TX_getLength

 Parameters
   uint8_t fID

 Returns
    uint8_t  XBeePacketLen[fID]

 Description
   add your description here
 Notes

 Author

 ****************************************************************************/
uint8_t TX_getLength(void) 
{
    return XBeePacketLen;
}

/****************************************************************************
 Function
   FieldTXIntResponse

 Parameters
   none

 Returns
    none

 Description
   TX UART interrupt response called from UART3 interrupt response
 Notes

 Author

 ****************************************************************************/
void FieldTXIntResponse(void) 
{
    // clear the TX intrerrupt:  Set TXIC in UARTICR to clear TX interrupt (Page 933 & 934)
    HWREG(UART3_BASE + UART_O_ICR) = UART_ICR_TXIC;
    
    if (bytesSent == bufferLen) // If we've sent all bytes of the packet
    {
        // Post ES_TX_SEND_COMPLETE to this service
        ES_Event TXEvent;
        TXEvent.EventType = ES_TX_SEND_COMPLETE;
        TXEvent.EventParam = 0;
        PostFieldTXService(TXEvent);
    } 
    else //else we still have bytes to send
    {
        HWREG(UART3_BASE + UART_O_DR) = XBeeTXBuffer[bytesSent]; // Write new data to register (UARTDR: UART data register) 
        bytesSent++;
    }
}

/***************************************************************************
 private functions
 ***************************************************************************/
/****************************************************************************
 Function
    sendStatusUpdate

 Parameters
   none

 Returns
   none

 Description
   Sends a broadcast message 
 Notes

 Author

 ****************************************************************************/
static void ConstructBroadcastMsg() {
	
  //Form the message
  XBeePacket[START_DELIMIER_OFFSET] 				= START_DELIMITER;
	XBeePacket[LENGTH_MSB_OFFSET] 						= LENGTH_MSB;
	XBeePacket[LENGTH_LSB_OFFSET] 						= LENGTH_LSB;
	XBeePacket[FRAME_TYPE_OFFSET] 						= API_TX_REQ;
	XBeePacket[FRAME_ID_OFFSET] 							= FRAME_ID;
	XBeePacket[DESTINATION_MSB_OFFSET] 				= BROADCAST_ADDRESS;
	XBeePacket[DESTINATION_LSB_OFFSET] 				= BROADCAST_ADDRESS;
	XBeePacket[OPTIONS_OFFSET] 								= OPTIONS;
	XBeePacket[DATA_OFFSET + STATUS_ADDRESS] 	= QueryFieldGameStatus();
	XBeePacket[DATA_OFFSET + C1MLOC1_ADDRESS] = QueryC1MLoc1();
	XBeePacket[DATA_OFFSET + C1MLOC2_ADDRESS] = QueryC1MLoc2();
	XBeePacket[DATA_OFFSET + C2MLOC1_ADDRESS] = QueryC2MLoc1();
	XBeePacket[DATA_OFFSET + C2MLOC2_ADDRESS] = QueryC2MLoc2();
	XBeePacket[DATA_OFFSET + C1RESH_ADDRESS] 	= SixteenToEightMSB(QueryC1Res());
	XBeePacket[DATA_OFFSET + C1RESL_ADDRESS] 	= SixteenToEightLSB(QueryC1Res());
	XBeePacket[DATA_OFFSET + C2RESH_ADDRESS] 	= SixteenToEightMSB(QueryC2Res());
	XBeePacket[DATA_OFFSET + C2RESL_ADDRESS] 	= SixteenToEightLSB(QueryC2Res());
	XBeePacket[DATA_OFFSET + PUR1_ADDRESS] 		= QueryPUR1();
	XBeePacket[DATA_OFFSET + PUR2_ADDRESS] 		= QueryPUR2();
  XBeePacket[CHECKSUM_OFFSET] 							= TX_getChecksum();

    
}
/****************************************************************************
 Function
    SixteenToEightLSB

 Parameters
   int16_t Byte

 Returns
   none

 Description
   
 Notes

 Author
KJ & MS   5:22pm    2/18/19 

 ****************************************************************************/

static int8_t SixteenToEightLSB(int16_t Byte) {
  uint8_t LSByte;
  LSByte = Byte & 0xFF;
 
  return LSByte;
}  
/****************************************************************************
 Function
    SixteenToEightMSB

 Parameters
   int16_t Byte

 Returns
   none

 Description
    
 Notes

 Author
KJ & MS   5:22pm    2/18/19 

 ****************************************************************************/
static int8_t SixteenToEightMSB(int16_t Byte) {
  uint8_t MSByte;
  MSByte = (Byte >> 8) & 0xFF; 
  
   return MSByte;
} 

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

