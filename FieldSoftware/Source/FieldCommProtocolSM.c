/****************************************************************************
 Module
   FieldCommProtocolSM.c

 Revision
   1.0.1

 Description
   Field Transmission Service
 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
 02/03/18       hr       edits for 2018 b field, comments to legacy code
 ****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
 */
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "FieldStatus.h"
#include "TimeService.h"

#include "Field_TXSM.h"
#include "FieldCommProtocolSM.h"
#include "sci.h"

/*----------------------------- Module Defines ----------------------------*/
#define FIELD_DIRECT_MSG_ROBOTS 0 //0 if the field doesn't message the robots directly

#define PROTOCOL_DEBUG_FLAG  0
#define PROTOCOL_DEBUG if(PROTOCOL_DEBUG_FLAG) printf

#define STATUS_UPDATE_PERIOD 333       // milliseconds

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
 */



/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/

/****************************************************************************
 Function
     InitFieldCommProtocolService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
 ****************************************************************************/
bool InitFieldCommProtocolService(uint8_t Priority) {
    ES_Event ThisEvent;

    MyPriority = Priority;

    // post the initial transition event
    ThisEvent.EventType = ES_INIT;
    if (ES_PostToService(MyPriority, ThisEvent) == true) {
        return true;
    } else {
        return false;
    }
}

/****************************************************************************
 Function
     PostFieldCommProtocolServic

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
 ****************************************************************************/
bool PostFieldCommProtocolService(ES_Event ThisEvent) {
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunFieldCommProtocolService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
 ****************************************************************************/
ES_Event RunFieldCommProtocolService(ES_Event ThisEvent) {
    ES_Event ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
    
    if(ThisEvent.EventType == ES_INIT) //if we receive ES_INIT event
    {
        ES_Timer_InitTimer(STATUS_TIMER, STATUS_UPDATE_PERIOD); //start status update timer
    }
    //if we receive a timeout from the status timer
    else if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == STATUS_TIMER)
    {
        //post an event to transmit service
        ES_Event TXResponse;
        TXResponse.EventType = ES_TX_REQUEST_SEND;
        
        //sendStatusUpdate(); //send a status update broadcast
        ES_Timer_InitTimer(STATUS_TIMER, STATUS_UPDATE_PERIOD); //restart timer
    }
    
    return ReturnEvent;
}

/***************************************************************************
     private functions
***************************************************************************/

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

