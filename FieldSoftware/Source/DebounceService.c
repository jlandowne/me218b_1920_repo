/****************************************************************************
 Module
   TimeService.c

 Description
   Main state machine for game control

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 16/01/28               dy          first draft
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for the framework and this service
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"  // Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "ES_ShortTimer.h"

#include "FieldStatus.h"
#include "FieldGameService.h"
#include "DebounceService.h"

#include <math.h>

/*----------------------------- Module Defines ----------------------------*/

#define DEBOUNCE_TIME 25

#define ALL_BITS (0xff<<2)
#define RED_BUCKET BIT5HI
#define BLUE_BUCKET BIT4HI

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

void initAttackAdBucketPins(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

static uint8_t lastPortRead;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitSuperPAC

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     intial control used for game control
 Notes

 Author
     DY
****************************************************************************/
bool InitDebounceService(uint8_t Priority) {
//  ES_Event ThisEvent;

    MyPriority = Priority;

    initAttackAdBucketPins();

    ES_Timer_InitTimer(DEBOUNCE_TIMER, DEBOUNCE_TIME);

    return true;
}

/****************************************************************************
 Function
     PostSuperPAC

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     DY
****************************************************************************/
bool PostDebounceService(ES_Event ThisEvent) {
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunSuperPAC

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
Events: ES_GAME_Start, ES_GAME_Pause, ES_GAME_End, ES_ChangeFrequency, ES_AttackEd
ES_Timeout: Game_Timer, Attack_Timer
 Notes

 Author
   DY
****************************************************************************/
ES_Event RunDebounceService(ES_Event ThisEvent) {
    ES_Event ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

    if (ThisEvent.EventType == ES_TIMEOUT) {
        if (ThisEvent.EventParam == DEBOUNCE_TIMER) {
            uint8_t thisPortRead =  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & (RED_BUCKET | BLUE_BUCKET);

            //printf("%d\n\r", thisPortRead);

            if (thisPortRead != lastPortRead) {
                ES_Event ThisEvent;
                
            }

            ES_Timer_InitTimer(DEBOUNCE_TIMER, DEBOUNCE_TIME);
            lastPortRead = thisPortRead;
        }
    }

    return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

void initAttackAdBucketPins(void) {
    // Enable the clock to Port B
    HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;

    // Wait for the Port B GPIO to be ready
    while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1);

    // Enable PB4 and PB5 for digital I/O
    HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT4HI | BIT5HI);

    // Set as inputs
    HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= ~(BIT4HI | BIT5HI);

    // Enable pulldown resistors
    HWREG(GPIO_PORTB_BASE + GPIO_O_PDR) |= (BIT4HI | BIT5HI);
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

