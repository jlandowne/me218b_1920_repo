/****************************************************************************
 Module
   EventCheckers.c

 Revision
   1.0.1

 Description
   This is the sample for writing event checkers along with the event
   checkers used in the basic framework test harness.

 Notes
   Note the use of static variables in sample event checker to detect
   ONLY transitions.

 History
 When           Who     What/Why
 -------------- ---     --------
 08/06/13 13:36 jec     initial version
****************************************************************************/

// this will pull in the symbolic definitions for events, which we will want
// to post in response to detecting events
#include "ES_Configure.h"
// this will get us the structure definition for events, which we will need
// in order to post events in response to detecting events
#include "ES_Events.h"
// if you want to use distribution lists then you need those function
// definitions too.
#include "ES_PostList.h"
// This include will pull in all of the headers from the service modules
// providing the prototypes for all of the post functions
#include "ES_ServiceHeaders.h"
// this test harness for the framework references the serial routines that
// are defined in ES_Port.c
#include "ES_Port.h"
// include our own prototypes to insure consistency between header &
// actual functionsdefinition
#include "ReloaderEventCheckers.h"

#include "FieldGameService.h"
#include "DisplayService.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"

#define ALL_BITS (0xff<<2)
#define RELOAD_IN_RED BIT2HI // in from red reloader
#define RELOAD_IN_BLUE BIT3HI //in from blue reloader


/*---------------------------- Module Variables ---------------------------*/
static uint8_t LastRedReloadState = 0; //variable stores previous state of red reload line
static uint8_t LastBlueReloadState = 0; //variable stores previous state of blue reload line

/****************************************************************************
 Function
   Check4ReloadPulse_Red
 Parameters
   None
 Returns
   bool: true if a new key was detected & posted
 Description
   checks for a rising edge pulse from red reload station
 Notes
   
 Author
   H Ravichandran
****************************************************************************/
bool Check4ReloadPulse_Red(void) {
    //Local ReturnVal = False
	bool ReturnVal = false;
	uint8_t CurrentRedReloadState; //stores the current value of reload line
	//Set Current ReloadState to state read from port pin
	CurrentRedReloadState = (HWREG(GPIO_PORTA_BASE+(GPIO_O_DATA+ALL_BITS)) & RELOAD_IN_RED);
    //if the line was low and is now high means reloaded
    if ((LastRedReloadState == 0) && (CurrentRedReloadState != LastRedReloadState)) 
    { 
        //post reload event to game service
        ES_Event Event2Post;
//      Event2Post.EventType = ES_RELOADED_RED;
        PostFieldGameService(Event2Post);
        PostDisplayService(Event2Post);
    }
    //Set LastState to the CurrentState
	LastRedReloadState = CurrentRedReloadState;
    return ReturnVal;
}

/****************************************************************************
 Function
   Check4ReloadPulse_Blue
 Parameters
   None
 Returns
   bool: true if a new key was detected & posted
 Description
   checks for a rising edge pulse from blue reload station
 Notes
   
 Author
   H Ravichandran
****************************************************************************/
bool Check4ReloadPulse_Blue(void) {
    //Local ReturnVal = False
	bool ReturnVal = false;
	uint8_t CurrentBlueReloadState; //stores the current value of reload line
	//Set Current ReloadState to state read from port pin
	CurrentBlueReloadState = (HWREG(GPIO_PORTA_BASE+(GPIO_O_DATA+ALL_BITS)) & RELOAD_IN_BLUE);
    //if the line was low and is now high means reloaded
    if ((LastBlueReloadState == 0) && (CurrentBlueReloadState != LastBlueReloadState)) 
    { 
        //post reload event to game service
        ES_Event Event2Post;
//        Event2Post.EventType = ES_RELOADED_BLUE;
        PostFieldGameService(Event2Post);
        PostDisplayService(Event2Post);
    }
    //Set LastState to the CurrentState
	LastBlueReloadState = CurrentBlueReloadState;
    return ReturnVal;
}

