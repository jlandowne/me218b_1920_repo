/****************************************************************************
 Module
   FieldGameService.c

 Description
   Main state machine for game control

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/28/16       dy      first draft
 02/03/18       hr      edits 2018 field, comments to the legacy code
 02/15/19		ms		edits 2019 field, comments to last year's code
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for the framework and this service
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_nvic.h"
#include "inc/hw_gpio.h"
#include "inc/hw_timer.h"

#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/pin_map.h"  // Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "ES_ShortTimer.h"
#include "inc/hw_types.h"
#include "inc/hw_ssi.h"

#include "FieldGameService.h"
#include "Field_TXSM.h"
#include "DisplayService.h"
#include "MinerService.h"
#include "LEDService.h"

/*----------------------------- Module Defines ----------------------------*/
#define ALL_BITS (0xff<<2)

/* Please note: due to variable name inertia, team Red will be team North
	 and team Blue will be team South.  */
enum PermitLocations {RED, BLUE, NEUTRAL1, NEUTRAL2}; 

#define FW_TICKS_PER_01S 	100 //framework clock freq 
#define RT_TICKS_PER_MS 	(40000/2) //ticks per ms in regulation timer clock //prescaler of 01 scales by 2
#define RT_PRESCALER 			0x0001 //prescaler used for rt prescaler

//time defines
#define TIE_BREAK_TIME     (218*100) //21.8s
#define GAME_OVER_TIME     (10*1000) //10s
#define REGULATION_TIME (138ULL*1000ULL)//138s
#define WAITING2PLAY_TIME (2) // 1ms less than Player Query time of 2ms

//#define DEBUG_FIELD_GAME_SERVICE

//Game status byte defines
#define SB_WAITINGFORPERMITS 	0x00
#define SB_PERMITSISSUED 			0x01
#define SB_PERMITSEXPIRED 		0x03

#define RED 0
#define BLUE 1
#define TIE 0xFF


#define NUM_COLORS 16
#define COLOR_CHANGE_PERIOD 30000UL
#define NUM_MINERS 4

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

static void InitOneShotRegulationTimer(void);
static void StartOneShotRegulationTimer( void );

static void StopOneShotRegulationTimer( void );

static void StopGameTimers(void);

static void SetRandomSquares(void);

static void ResetGame(void);

static bool IsCurrentMinerLocation(uint8_t);


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

static FieldGameState_t CurrentState;

static uint16_t RegulationStartTime = 0; //stores time when regulation timer was started
static uint16_t TieBreakStartTime = 0; //stores time when tie break timer was started

static uint8_t Winner; //variable to store winner

ScoringLocations ScoringSquares;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitFieldGameService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     intial control used for game control
 Notes

 Author
     DY
****************************************************************************/
bool InitFieldGameService(uint8_t Priority) 
{
    MyPriority = Priority;

    //Init regulation one shot
    InitOneShotRegulationTimer();
    
    CurrentState = WaitingForPermits; //set current state to wait 2 play
    ES_Timer_InitTimer(WAITING2PLAY_TIMER, WAITING2PLAY_TIME);
    return true;
}

/****************************************************************************
 Function
     PostFieldGameService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     DY
****************************************************************************/
bool PostFieldGameService(ES_Event ThisEvent) 
{
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunFieldGameService

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description

 Notes

 Author
   HRavichandiran
 Revised
	 MSu
****************************************************************************/
ES_Event RunFieldGameService(ES_Event ThisEvent) 
{
    ES_Event ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
    switch (CurrentState) 
    {
        case WaitingForPermits: //if current state is waiting 2 play
        {
          if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == WAITING2PLAY_TIMER)
          {
            //SetRandomSquares();
            ES_Timer_InitTimer(WAITING2PLAY_TIMER, WAITING2PLAY_TIME);
          }
			
          if(ThisEvent.EventType == ES_GAME_START) //if we get the game start event
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: ES_GAME START\n\r");
                #endif
							// Start countdown timer, and begin game after it expires
							ES_Timer_InitTimer(COUNTDOWN_TIMER, COUNTDOWN_TIME);
							// Pass the ES_GAME_START event to LEDService for it to begin its countdown
							PostLEDService(ThisEvent);
            }
						else if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == COUNTDOWN_TIMER) {
							//ES_Timer_InitTimer(CHANGE_COLOR_SQUARE_TIMER, COLOR_CHANGE_PERIOD);
							SetRandomSquares();
							StartOneShotRegulationTimer(); //start the regulation timer
							ES_Event Event = {ES_GAME_START, 0};
							PostLEDService(Event);
						 
							CurrentState = PermitsIssued; //change state to InPlay
						}
        }
        break;
				
        case PermitsIssued: // if current state is in play
        {
            switch(ThisEvent.EventType)
            {
              case ES_TIMEOUT:
								if(ThisEvent.EventParam == CHANGE_COLOR_SQUARE_TIMER) {
									#ifdef DEBUG_FIELD_GAME_SERVICE
											printf("RunGame: ES_CHANGE_COLOR\n\r");
									#endif
									SetRandomSquares();
									//ES_Timer_InitTimer(CHANGE_COLOR_SQUARE_TIMER, COLOR_CHANGE_PERIOD);
								}
              break; 
							
							case ES_CHANGE_COLOR:
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame: ES_CHANGE_COLOR\n\r");
                #endif
                SetRandomSquares();
								//ES_Timer_InitTimer(CHANGE_COLOR_SQUARE_TIMER, COLOR_CHANGE_PERIOD);
              break; 
              
              case ES_RESET:
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: ES_RESET\n\r");
                #endif
                
                CurrentState = WaitingForPermits; //change state to Waiting2Play
              break;

              case ES_REGULATION_TIMEOUT:
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame: ES_REGULATION_TIMEOUT\n\r");
                #endif
                CurrentState = PermitsExpired;
              break;             
            }//end switch              
        }//end Cleaning up
        break;

        case PermitsExpired:
        {
            //if we get a timeout from the game over timer
            if(ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == GAME_OVER_TIMER)
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                    printf("RunGame: Game over timeout, back to wait 2 play\n\r");
                #endif
								ResetGame();
                CurrentState = WaitingForPermits; //move to waiting 2 play
                ES_Timer_InitTimer(WAITING2PLAY_TIMER, WAITING2PLAY_TIME);
            }
            else if(ThisEvent.EventType == ES_RESET) //if we get the game reset event
            {
                #ifdef DEBUG_FIELD_GAME_SERVICE
                printf("RunGame: ES_RESET\n\r");
                #endif
                ResetGame();
                CurrentState = WaitingForPermits; //change state to Waiting2Play
                ES_Timer_InitTimer(WAITING2PLAY_TIMER, WAITING2PLAY_TIME);
            }
        }
        break;

        default :
            break;
    }//end state switch
    
            
    return ReturnEvent;
}
/****************************************************************************
 Function
    QueryFieldGameServiceState

 Parameters
   none

 Returns
   uint8_t

 Description
    
 Notes

 Author
   Harine Ravichandiran, 02/04/18
****************************************************************************/
FieldGameState_t QueryFieldGameState(void)
{
    return CurrentState; //return the state
}
/****************************************************************************
 Function
    QueryRegulationStartTime

 Parameters
   none

 Returns
   uint16_t

 Description
    
 Notes

 Author
   Harine Ravichandiran, 02/04/18
****************************************************************************/
uint16_t QueryRegulationStartTime(void)
{
    return RegulationStartTime; //return the timer start time
}
/****************************************************************************
 Function
    QueryTieBreakStartTime

 Parameters
   none

 Returns
   uint16_t

 Description
    
 Notes

 Author
   Harine Ravichandiran, 02/04/18
****************************************************************************/
uint16_t QueryTieBreakStartTime(void)
{
    return TieBreakStartTime; //return the timer start time
}
/****************************************************************************
 Function
    QueryWinner

 Parameters
   none

 Returns
   uint8_t

 Description
    
 Notes

 Author
   Harine Ravichandiran, 02/04/18
****************************************************************************/
uint8_t QueryWinner(void)
{
	return (GetRedScore() > GetBlueScore()) ? RED : 
		(GetRedScore() < GetBlueScore()) ? BLUE : TIE ;
}


/****************************************************************************/

/****************************************************************************
 Function
    QueryFieldGameStatus

 Parameters
   none

 Returns
   uint8_t

 Description
    returns the status byte corresponding to the field status

 Author
   Harine Ravichandiran, 02/04/18
   Mimi Su,	02/15/18
****************************************************************************/
uint8_t QueryFieldGameStatus(void) 
{
    //Set the bytes depending on the current state of state machine
    //WaitingForPermits, PermitsIssued, PermitsExpired
    switch(CurrentState)
    {
        case WaitingForPermits:
					return SB_WAITINGFORPERMITS;
        case PermitsIssued:
					return SB_PERMITSISSUED;
        case PermitsExpired:
					return SB_PERMITSEXPIRED;
        default:
					return 0x00;
    }
}

/****************************************************************************
 Function
    QueryPUR1

 Parameters
   none

 Returns
   uint8_t

 Description
    returns available mining locations exclusive to red and blue

 Author
   J. Landowne
****************************************************************************/
uint8_t QueryPUR1(void) {
	return (ScoringSquares.BluLoc << 4) | (ScoringSquares.RedLoc & 0xFF);
}

/****************************************************************************
 Function
    QueryPUR1

 Parameters
   none

 Returns
   uint8_t

 Description
    returns available mining locations exclusive to red and blue

 Author
   J. Landowne
****************************************************************************/
uint8_t QueryPUR2(void) {
	return (ScoringSquares.Neut2Loc << 4) | (ScoringSquares.Neut1Loc & 0xFF);
}

/****************************************************************************
 Function
    QuerPUR1

 Parameters
   none

 Returns
   uint8_t

 Description
    returns available mining locations exclusive to red and blue

 Author
   J. Landowne
****************************************************************************/
uint8_t QueryPermitLocations(uint8_t permit) {
	switch (permit) {
		case RED:
			return ScoringSquares.RedLoc;
		case BLUE:
			return ScoringSquares.BluLoc;
		case NEUTRAL1:
			return ScoringSquares.Neut1Loc;
		case NEUTRAL2:
			return ScoringSquares.Neut2Loc;
		default:
			return 0x00;
   }
}

/****************************************************************************
 Function
    RegualtionTimerOneShotISR

 Parameters
     none

 Returns
     none

 Description
	ISR: Wtimer4A to Cycle One shot timer
 Notes

 Author
     H.Ravichandiran
****************************************************************************/
void RegualtionTimerOneShotISR( void ) 
{
	// clear interrupt source
	HWREG(WTIMER4_BASE+TIMER_O_ICR) = TIMER_ICR_TATOCINT;
    // stop the regulation timer
    StopOneShotRegulationTimer();
	
	//Post ES_REGULATION_TIMEOUT Event 
	ES_Event Event2Post;
	Event2Post.EventType = ES_REGULATION_TIMEOUT;
	PostFieldGameService(Event2Post);
    //update the display service
    Event2Post.EventType = ES_RESET_REGULATION_TIMER;
    PostDisplayService(Event2Post);
}
/****************************************************************************
 Function
    GetScoringSquares

 Parameters
     none

 Returns
     none

 Description
		Returns struct of location of scoring locatins (red, blue, neutral 1 and 2)
 Notes

 Author
     H.Ravichandiran
****************************************************************************/

ScoringLocations GetScoringLocations(void) {
	return ScoringSquares;
}

/***************************************************************************
 private functions
 ***************************************************************************/
/****************************************************************************
 Function
    InitOneShotRegulationTimer

 Parameters
     none

 Returns
     none

 Description
     Initializes Wtimer4A One shot timer to 138s for gameplay time
 Notes

 Author
     H.Ravichandiran
****************************************************************************/
static void InitOneShotRegulationTimer(void)
{
// start by enabling the clock to the timer ( Timer 4)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R4;
    
	//Wait for clock to be ready
  while ((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R4) != SYSCTL_PRWTIMER_R4)
    ;
	//prescale the WTIME4A to a prescaler to divide by 2
  HWREG(WTIMER4_BASE+TIMER_O_TAPR ) = RT_PRESCALER; 
  
	// make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER4_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
	
	// set it up in 16 bit wide (individual, not concatenated) mode
  HWREG(WTIMER4_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
	
	// set up timer A in one-shot mode so that it disables time-outs
  HWREG(WTIMER4_BASE+TIMER_O_TAMR) =
  (HWREG(WTIMER4_BASE+TIMER_O_TAMR)& ~TIMER_TAMR_TAMR_M)|
  TIMER_TAMR_TAMR_1_SHOT;
	
	// set timeout
  HWREG(WTIMER4_BASE+TIMER_O_TAILR) = REGULATION_TIME * RT_TICKS_PER_MS; 

	// disable a local timeout interrupt
    HWREG(WTIMER4_BASE+TIMER_O_IMR) &= ~(TIMER_IMR_TATOIM);
	
	// enable the Timer A in Timer 4 interrupt in the NVIC
	// it is interrupt number 102
  HWREG(NVIC_EN3) |= BIT6HI;
	
// make sure interrupts are enabled globally
  __enable_irq();
	
	#ifdef DEBUG_FIELD_GAME_SERVICE
	printf("Cycle One Shot: Init Done \r\n");
	#endif
}

/****************************************************************************
 Function
    StartOneShotRegulationTimer

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Kick-off the one shot timer

 Author
     H.Ravichandiran
****************************************************************************/
static void StartOneShotRegulationTimer( void ) {
	// clear interrupt source
	HWREG(WTIMER4_BASE+TIMER_O_ICR) = TIMER_ICR_TATOCINT;
	// Set the timer register value back 
	//Since we are counting back
	HWREG(WTIMER4_BASE+TIMER_O_TAV) = REGULATION_TIME * RT_TICKS_PER_MS;
	//Enable interrupt
	HWREG(WTIMER4_BASE+TIMER_O_IMR) |= TIMER_IMR_TATOIM;
	// kickoff the timer
    HWREG(WTIMER4_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
    //update the regulation start time variable in the display service by posting an event
    ES_Event Event2Post;
    Event2Post.EventType = ES_START_REGULATION_TIMER;
    PostDisplayService(Event2Post);
}

/****************************************************************************
 Function
    StopOneShotRegulationTimer

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   Stop the one shot timer

 Author
     H.Ravichandiran
****************************************************************************/
static void StopOneShotRegulationTimer( void ) {
	// stop the timer
    HWREG(WTIMER4_BASE+TIMER_O_CTL) &= (~TIMER_CTL_TAEN);
    //update the regulation start time variable in the display service by posting an event
    ES_Event Event2Post;
    Event2Post.EventType = ES_RESET_REGULATION_TIMER;
    PostDisplayService(Event2Post);
}

/*------------------------------- Footnotes -------------------------------*/

/****************************************************************************
 Function
    StopGameTimers

 Parameters
     none

 Returns
     none

 Description
     Stops the pertinent game timers when moving back to waiting state

 Notes

 Author
     H.Ravichandiran
****************************************************************************/
static void StopGameTimers(void)
{
    // stop all game timers to be safe
    ES_Timer_StopTimer(SHOT_CLOCK_TIMER); //Stop the Shot Clock timer
    ES_Timer_StopTimer(TIE_BREAK_TIMER); //Stop the Tie break timer
    ES_Timer_StopTimer(GAME_OVER_TIMER); //Stop the Game over timer
    
    // stop the regulation timer
    StopOneShotRegulationTimer();
}

/****************************************************************************
 Function
    ResetGame

 Parameters
     none

 Returns
     none

 Description
    Resets the game parameters, such as locations and score.

 Notes

 Author
     J. Landowne
****************************************************************************/
static void ResetGame(void)
{
			StopGameTimers();            //stop all game timers
			RegulationStartTime = 0;     //zero out start times
			ResetScores();
}

/****************************************************************************
 Function
    IsCurrentMinerLocation

 Parameters
     none

 Returns
     none

 Description
    Returns if value is a current miner location (0-15)

 Notes

 Author
     J. Landowne
****************************************************************************/
static bool IsCurrentMinerLocation(uint8_t loc)
{
	for (uint8_t i = 0; i < NUM_MINERS; i++) {
			if(loc == GetMinerLocation(i)) {
				return true;
			}
	}

	return false;
}

/****************************************************************************
 Function
    GetRandomSquare

 Parameters
   none

 Returns
   uint8_t

 Description
   picks random colors for scoring locations. Will not pick current miner locations,
		or repeat the same location for the same square. Randomization
		seed is based on SysTick.
 Notes

 Author
		J. Landowne
****************************************************************************/
static void SetRandomSquares(void){
	// function variables
	uint8_t R_idx;
	uint8_t B_idx;
	uint8_t N1_idx;
	uint8_t N2_idx;
	static uint8_t LastR_idx;
	static uint8_t LastB_idx;
	static uint8_t LastN1_idx;
	static uint8_t LastN2_idx; 
	
	srand(SysTickValueGet());
	//	generate a random color for West RCYC Center
	do {
		R_idx = (rand() % NUM_COLORS);
	} while (R_idx == LastR_idx || IsCurrentMinerLocation(R_idx));

	do {
		B_idx = (rand() % NUM_COLORS);
	} while(R_idx == B_idx || B_idx == LastB_idx || IsCurrentMinerLocation(B_idx));
	
	do {
		N1_idx = (rand() % NUM_COLORS);
	} while(N1_idx == R_idx || N1_idx == B_idx || N1_idx == LastN1_idx || IsCurrentMinerLocation(N1_idx));
	
	do {
		N2_idx = (rand() % NUM_COLORS);
	} while(N2_idx == R_idx || N2_idx == B_idx || N2_idx == N1_idx || N2_idx == LastN2_idx || IsCurrentMinerLocation(N2_idx));
	
	ScoringSquares.BluLoc = B_idx;
	ScoringSquares.RedLoc = R_idx;
	ScoringSquares.Neut1Loc = N1_idx;
	ScoringSquares.Neut2Loc = N2_idx;
	
	LastR_idx = R_idx;
	LastB_idx = B_idx;
	LastN1_idx = N1_idx;
	LastN2_idx = N2_idx;
	
	ES_Event PostEvent; //Tell LEDs about location update
	PostEvent.EventType = ES_LOCATION_UPDATE;
	PostLEDService(PostEvent);
	
	#ifdef DEBUG_FIELD_GAME_SERVICE
		printf("Locations of Scoring Squares \r\n");
		printf("R: %d \r\n",ScoringSquares.RedLoc);
		printf("B: %d \r\n",ScoringSquares.BluLoc);
		printf("N1: %d \r\n",ScoringSquares.Neut1Loc);
		printf("N2: %d \r\n\r\n",ScoringSquares.Neut2Loc);
	#endif
}

 

/*------------------------------ End of file ------------------------------*/      
