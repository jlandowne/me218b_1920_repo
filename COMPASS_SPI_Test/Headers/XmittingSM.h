/****************************************************************************
 Header file for XmittingSM
 02/08/12 adjsutments for use with the Events and Services Framework Gen2
 3/17/09  Fixed prototpyes to use Event_t
 ****************************************************************************/

#ifndef XmittingSM_H
#define XmittingSM_H


// typedefs for the states
// State definitions for use with the query function
typedef enum { Waiting2Send, Sending5Bytes } XmittingState_t ;


// Public Function Prototypes

ES_Event RunXmittingSM( ES_Event CurrentEvent );
void StartXmittingSM ( ES_Event CurrentEvent );
XmittingState_t QueryXmittingSM ( void );

//Query fn for LOCResponse
uint8_t QueryXmitLOCResponse ( uint8_t index );

#endif /*XmittingSM_H */