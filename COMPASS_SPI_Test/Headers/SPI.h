/****************************************************************************
 
  Header file for SPI

 ****************************************************************************/

#ifndef SPI_H
#define SPI_H

#include "ES_Configure.h"
#include "ES_Types.h"

// Public Function Prototypes
void InitSPI(void);
void SendCommand( uint8_t Data );
uint8_t ReadData(void);

#endif /* SPI_H */
