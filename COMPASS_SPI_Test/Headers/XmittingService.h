
/****************************************************************************

  Header file for XmittingService
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef XmittingService_H
#define XmittingService_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h" //event struct definition

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  Wait2Xmit, Xmitting
}XmittingState_t;

// Public Function Prototypes

bool InitXmittingService(uint8_t Priority);
bool PostXmittingService(ES_Event_t ThisEvent);
ES_Event_t RunXmittingService(ES_Event_t ThisEvent);
XmittingState_t QueryXmittingService(void);

#endif /* XmittingService_H */

