#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# default configuration
CND_ARTIFACT_DIR_default=dist/default/production
CND_ARTIFACT_NAME_default=COMPASS_SW.production.hex
CND_ARTIFACT_PATH_default=dist/default/production/COMPASS_SW.production.hex
CND_PACKAGE_DIR_default=${CND_DISTDIR}/default/package
CND_PACKAGE_NAME_default=compasssw.tar
CND_PACKAGE_PATH_default=${CND_DISTDIR}/default/package/compasssw.tar
# HW_Stack configuration
CND_ARTIFACT_DIR_HW_Stack=dist/HW_Stack/production
CND_ARTIFACT_NAME_HW_Stack=COMPASS_SW.production.hex
CND_ARTIFACT_PATH_HW_Stack=dist/HW_Stack/production/COMPASS_SW.production.hex
CND_PACKAGE_DIR_HW_Stack=${CND_DISTDIR}/HW_Stack/package
CND_PACKAGE_NAME_HW_Stack=compasssw.tar
CND_PACKAGE_PATH_HW_Stack=${CND_DISTDIR}/HW_Stack/package/compasssw.tar
# Managed_Stack configuration
CND_ARTIFACT_DIR_Managed_Stack=dist/Managed_Stack/production
CND_ARTIFACT_NAME_Managed_Stack=COMPASS_SW.production.hex
CND_ARTIFACT_PATH_Managed_Stack=dist/Managed_Stack/production/COMPASS_SW.production.hex
CND_PACKAGE_DIR_Managed_Stack=${CND_DISTDIR}/Managed_Stack/package
CND_PACKAGE_NAME_Managed_Stack=compasssw.tar
CND_PACKAGE_PATH_Managed_Stack=${CND_DISTDIR}/Managed_Stack/package/compasssw.tar
