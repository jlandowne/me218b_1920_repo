/* 
 * File:   COMMDEFS.h
 * Author: TAOffice
 *
 * Created on February 8, 2018, 11:10 AM
 */

#ifndef COMMDEFS_H
#define	COMMDEFS_H

#ifdef	__cplusplus
extern "C" {
#endif

#define FRAME_BYTE 0x7E
#define MSB        0
#define API_RX_DATA 0x81
#define API_TX_STAT 0x89
#define VALID_CKSUM 0xFF

#define MSG_SIZE       16   // Maximum length of a received message from the XBee, set by COMM protocol
                            // 11 bytes Payload + 1 byte API Identifier + 1 byte RSSI + 1 byte Options 

#define NORTH_TEAM_SIZE 1
#define SOUTH_TEAM_SIZE 1
#define DATA_SIZE 11        //Size of Data Payload from Field (Registers 0x00 through 0xA0)

// Define offset into the message bytes
    
// RX Offsets //within the data part fo the packet
#define API_INDENTIFIER_INDEX 0        // Offset to API type byte
#define FIELD_ADDRESS_MSB_INDEX 1 //MSB of field address
#define FIELD_ADDRESS_LSB_INDEX 2    //LSB of field address
#define RSSI_INDEX     3
#define OPTIONS_INDEX 4 //bit 0, reserved, bit 1 address broadcast, bit 2 PAN broadcast, bit 3-7 reserved
#define DATA_INDEX  OPTIONS_INDEX+1

//Rx masks
#define BROADCAST_MASK (BIT2HI | BIT1HI)
    
// TX Offsets
#define FID_OFFSET 1        // Offset to frame ID
#define STAT_OFFSET 2       // offset to TX Status byte

#define ADH_SHIFT  8        // Shift required to address high byte


#ifdef	__cplusplus
}
#endif

#endif	/* COMMDEFS_H */

