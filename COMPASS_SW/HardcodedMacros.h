/* 
 * File:   HardcodedMacros.h
 * Author: TAOffice
 *
 * Created on February 4, 2016, 8:52 PM
 */

#ifndef HARDCODEDMACROS_H
#define	HARDCODEDMACROS_H

#define FastPostUARTRXService(X) ES_PostToService(0, X)
#define FastPostSPIService(X) ES_PostToService(1, X)

#endif	/* HARDCODEDMACROS_H */

